<%
Server.ScriptTimeout = 3600
session.LCID = 2057

yearBuiltVar = 2005
if year(now) > yearBuiltVar then
	yearNowVar = yearBuiltVar & "-" & year(now)
else
	yearNowVar = yearBuiltVar
end if


' ******************************************************************************************************************************************* '
' ** FUNCTION LIST ************************************************************************************************************************** '
' ******************************************************************************************************************************************* '
' ** Function hexValue(ByVal intHexLength) - creates a HEX value of string length *intHexLength* ******************************************** '
' ** Function sqlSafe(passthru,typeVar) - removes ' characters: typeVar = 1 then *HTMLENCODE*, typeVar = 2 then *URL ENCODE* **************** '
' ** Function requestSafe(passthru) - validates numeric variables *************************************************************************** '
' ** Function IsValidEmail(sEmail) - validates email addresses ****************************************************************************** '
' ** Function FireOffAnEmail - sends an email with this list of options. ******************************************************************** '
' **						   Only fromName,toAddress,subjectLine,emailMessage are essential, will not send without  these ***************** '
' **					fromAddress - sender email ****************************************************************************************** '
' **					fromName - sender's real name *************************************************************************************** '
' **					toAddress - recipient email ***************************************************************************************** '
' **					ccList - comma separated list of CC emails ************************************************************************** '
' **					bccList - comma separated list of BCC emails ************************************************************************ '
' **					subjectLine - email subject line ************************************************************************************ '
' **					isHTML - if message is HTML formatted this must be set to true ****************************************************** '
' **					altMessage - if isHTML is true this is the plaintext variant (must be included is isHTML is true) ******************* '
' **					emailMessage - email message (if isHTML is true this should be HTML formatted *************************************** '
' **					fileList  - list of filenames in this format: server.MapPath("filename") in an array (variable name = array name) *** '
' ** Function formDate(record_id, dayVar, dayStr, monthVar, monthStr, yearVar, yearStr) ***************************************************** '
' ** Function emailSafe(passthru) - allows formatted HTML text from a text editor to be passed through as a hidden field ******************** '
' ******************************************************************************************************************************************* '
' ******************************************************************************************************************************************* '

Function makeSafeName(filenameStr,extension)
	if len(filenameStr) = 0 then
		filenameStr = hexValue(8)
	end if
	if len(extension) = 0 then
		extension = ".asp"
	end if
	Set oRE = New Regexp
	oRE.Pattern = "[\W_]"
	oRE.Global = True
	filenameStr = oRE.Replace(filenameStr, "")
	makeSafeName = filenameStr & extension
End Function

Private Function hexValue(ByVal intHexLength)

	Dim intLoopCounter
	Dim strHexValue

	'Randomise the system timer
	Randomize Timer()

	'Generate a hex value
	For intLoopCounter = 1 to intHexLength

		'Genreate a radom decimal value form 0 to 15
		intHexLength = CInt(Rnd * 1000) Mod 16

		'Turn the number into a hex value
		Select Case intHexLength
			Case 1
				strHexValue = "1"
			Case 2
				strHexValue = "2"
			Case 3
				strHexValue = "3"
			Case 4
				strHexValue = "4"
			Case 5
				strHexValue = "5"
			Case 6
				strHexValue = "6"
			Case 7
				strHexValue = "7"
			Case 8
				strHexValue = "8"
			Case 9
				strHexValue = "9"
			Case 10
				strHexValue = "A"
			Case 11
				strHexValue = "B"
			Case 12
				strHexValue = "C"
			Case 13
				strHexValue = "D"
			Case 14
				strHexValue = "E"
			Case 15
				strHexValue = "F"
			Case Else
				strHexValue = "Z"
		End Select

		'Place the hex value into the return string
		hexValue = hexValue & strHexValue
	Next
End Function

function sqlSafe(passthru,typeVar)
	if len(passthru)>0 then
		if typeVar = "1" then
			sqlSafe = server.htmlencode(passthru)
		elseif typeVar = "2" then
			sqlSafe = replace(passthru,"'","%27")
		elseif typeVar = "3" then
			sqlSafe = replace(passthru,"'","")
		elseif typeVar = "4" then
			sqlSafe = replace(passthru,"'","''")
		else
			sqlSafe = replace(passthru,"'","&#146;")
		end if
	else
		sqlSafe = ""
	end if
end function

function requestSafe(passthru)
	stepthru = 0
	if IsEmpty(passthru) then
		stepthru = 1
	end if
	if IsNull(passthru) then
		stepthru = 1
	end if
	if passthru = "" then
		stepthru = 1
	end if
	if stepthru = 0 then
		if IsNumeric(CStr(passthru)) then
			stepthru = 0
		else
			stepthru = 1
		end if
	end if
	requestSafe = stepthru
end function

Function IsValidEmail(sEmail)
	IsValidEmail = false
	Dim regEx, retVal
	Set regEx = New RegExp
	' Create regular expression:
	regEx.Pattern ="^[\w-\.]{1,}\@([\da-zA-Z-]{1,}\.){1,}[\da-zA-Z-]{2,3}$"
	' Set pattern:
	regEx.IgnoreCase = true
	' Set case sensitivity.
	retVal = regEx.Test(sEmail)
	' Execute the search test.
	If not retVal Then
		exit function
	End If
	IsValidEmail = true
End Function

Function FireOffAnEmail(fromAddress,fromName,toAddress,ccList,bccList,subjectLine,isHTML,altMessage,emailMessage,fileList)
	Set Mail = Server.CreateObject("Persits.MailSender")
'	Mail.Host = "mail.nomoreart.com"
'	Mail.Username = "aspemail@nomoreart.com"
'	Mail.Password = "45p3m41l"
	Mail.Host = "mail.belfaststudio.com"
	Mail.Username = "persits@belfaststudio.com"
	Mail.Password = "persits"
'	Mail.Host = "smtp.web-solutions-ni.com"
'	Mail.Username = "aspemail@websol"
'	Mail.Password = "aspemail"
	sendMail = 1
	if len(fromAddress) > 0 then
		Mail.From = fromAddress
	else
		sendMail = 0
	end if
	if len(fromAddress) > 0 then
		Mail.FromName = fromName
	end if
	if len(toAddress) > 0 then
		Mail.AddAddress toAddress
	else
		sendMail = 0
	end if
	if len(ccList) > 0 then
		ccArr = split(ccList,",")
		for iCC = 0 to uBound(ccArr)
			if IsValidEmail(ccArr(iCC)) then
				Mail.AddCC ccArr(iCC)
			end if
		next
	end if
	if len(bccList) > 0 then
		bccArr = split(bccList,",")
		for iBCC = 0 to uBound(bccArr)
			if IsValidEmail(bccArr(iBCC)) then
				Mail.AddBCC bccArr(iBCC)
			end if
		next
	end if
	if len(subjectLine) > 0 then
		Mail.Subject = subjectLine
	else
		sendMail = 0
	end if
	if (isHTML) and len(altMessage) > 0 then
		Mail.IsHTML = True
		Mail.AltBody = altMessage
	end if
	if len(emailMessage) > 0 then
		Mail.Body = emailMessage
	else
		sendMail = 0
	end if
	if (isArray(fileList)) then
		for iFile = 0 to uBound(fileList)
			Mail.AddAttachment fileList(iFile)
		next
	end if
	if sendMail = 1 then
		Mail.Send
	end if
	Set Mail = Nothing
End Function

function formDate(record_id, dayVar, dayStr, monthVar, monthStr, yearVar, yearStr)

	response.write "<select name=""" & dayStr & """ class=""select_date"">" & VbCrLf

	if record_id="0" then
		for day_loop = 1 to 31
			response.write "<option"
			If day(now()) = day_loop Then
				response.write " selected"
			End If
			response.write ">" & day_loop & "</option>" & VbCrLf
		next
	else
		for day_loop = 1 to 31
			response.write "<option"
			If dayVar = CStr(day_loop) Then
				response.write " selected"
			End If
			response.write ">" & day_loop & "</option>" & VbCrLf
		next
	end if
	response.write "</select><select name=""" & monthStr & """ class=""select_date"">" & VbCrLf
	if record_id="0" then
		for month_loop = 1 to 12
			response.write "<option"
				If CStr(Month(now())) = CStr(month_loop) Then
					response.write " selected"
				end if
			response.write ">" & MonthName(month_loop,1) & "</option>" & VbCrLf
		next
	else
		for month_loop = 1 to 12
			response.write "<option"
				If CStr(monthVar) = CStr(month_loop) Then
					response.write " selected"
				end if
			response.write ">" & MonthName(month_loop,1) & "</option>" & VbCrLf
		next
	end if
	response.write "</select><select name=""" & yearStr & """ class=""select_date"">" & VbCrLf
	if record_id="0" then
		for year_loop = 2000 to (year(now())+3)
			response.write "<option"
			If year(now()) = year_loop Then
				response.write " selected"
			End If
			response.write ">" & year_loop & "</option>" & VbCrLf
		next
	else
		for year_loop = 2000 to (year(now())+3)
			response.write "<option"
			If CStr(yearVar) = CStr(year_loop) Then
				response.write " selected"
			End If
			response.write ">" & year_loop & "</option>" & VbCrLf
		next
	end if
	response.write "</select>" & VbCrLf

end function

function imageSafe(filename)
		if len(filename) > 0 then
			filename = replace(filename,"%20"," ")
			do while InStr(filename,"  ") > 0
				filename = replace(filename,"  "," ")
			loop
			filename = replace(filename," ","_")
			filename = replace(filename,"'","")
			filename = replace(filename,",","")
			filename = replace(filename,"""","")
			filename = replace(filename,"&","")
			filename = replace(filename,"\","")
			filename = replace(filename,"/","")
			filename = replace(filename,"?","")
			filename = replace(filename,";","")
			filename = replace(filename,":","")
			filename = sqlSafe(filename,3)
		end if
		imageSafe = filename
end function

function emailSafe(passthru)
	if len(passthru)>0 then
		emailSafe = replace(passthru,"""","'")
	else
		emailSafe = ""
	end if
end function
%>