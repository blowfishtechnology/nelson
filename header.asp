<!-- #include file="conex/ocnser.asp" -->
<!-- #include file="conex/date_function.asp" -->
<!--#Include file="conex/pageresolver.asp"-->

<%
if IsEmpty(PAGEID) then
 PageID = 1
end if

SQL="SELECT * FROM content WHERE id = " & PageID
Set RecordSet = Connection.Execute(SQL)
do while not RecordSet.EOF
	page_name = RecordSet("name")
	page_content = RecordSet("content")
    page_title = RecordSet("title")
    page_description = RecordSet("description")
    page_keywords = RecordSet("keywords")
    page_access_to_he = RecordSet("members_page")
	RecordSet.MoveNext
loop
RecordSet.Close
%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" />
<meta name="Description" content="" />
<meta name="Keywords" content="" />
<meta name="Author" content="" />
<meta name="Rating" content="General" />
<meta name="ROBOTS" content="ALL" />
<meta http-equiv="x-ua-compatible" content="IE=8">
<link rel="stylesheet" href="styles/default.css" />
<link rel="stylesheet" href="styles/forms.css" />	
<link rel="stylesheet" href="styles/global.css">
<title>Welcome to Nelson Hydraulics</title>

	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
	<script src="script/jquery.easing.1.3.js"></script>
	<script src="script/slides.min.jquery.js"></script>
	<script>
	    $(function () {
	        $('#slides').slides({
	            preload: true,
	            preloadImage: 'image/nh/loading.gif',
	            play: 9000,
	            pause: 2500,
	            hoverPause: true
	        });
	    });
	</script>

</head>
<body>

<div id="nh-container"><!-- Start of container -->

	<div class="nh-containercenter"><!-- Start of containercenter -->
	
		<div class="nh-containerheader"><!-- Start of containerheader -->
		
			<div class="nh-headerleft">
				<p>Welcome to Nelson Hydraulics</p>
				<a href="index.asp"><img src="images/nh/logo.png" title="Logo" alt="Logo" width="600" border="0" /></a>
			</div>
			
			<div class="nh-headerright">
				<p>
					Unit H1, Knockmore Industrial Estate, Lisburn, County Antrim
				</p>
				<p>
					Tel: 028 92662781
				</p>
				<p>
					<a href="mailto:info@nelsonhydraulics.com">info@nelsonhydraulics.com</a>
				</p>
			</div>
			
			<div style="clear: both;"></div>
			
		</div><!-- End of containerheader -->
		
		<div class="nh-containernavigation"><!-- End of containernavigation -->
		
			<div class="nh-navigation"><!-- Start of navigation -->
			

  <% 
Dim my_array, fullname, fname
'Response.Write Request.ServerVariables("SCRIPT_NAME") & "<br>" 
fullname = Request.ServerVariables("SCRIPT_NAME")
my_array=split(fullname,"/")
fname=my_array(ubound(my_array))
'Response.Write fname
 %>
				<div class="nh-navigationarea">
					<ul>
						<li><a href="index.asp" <% if fname = "index.asp" then %> class="active" <% end if %>>Home</a></li>
						<li><a href="products.asp" <% if fname = "products.asp" then %> class="active" <% end if %>>Products</a></li>
						<li><a href="content.asp?id=5"  <% if PageID = 5 then  %>class="active" <% end if %>>Hose Kits</a></li>
						<li><a href="content.asp?id=6" <% if PageID = 6 then  %>class="active" <% end if %>>Engineering Services</a></li>
						<li><a href="contact.asp" <% if fname = "contact.asp" then %> class="active" <% end if %>>Contact Us</a></li>
					</ul>
				</div>
				
				<div class="nh-search">
						
					<form method="post" action="search.asp" name="searchform" id="searchform">
						<div class="nh-searchform">
							<div class="nh-searchforminput">
						       	<input name="search" type="text" id="Search" size="30" maxlength="50" />
						    </div>
						    <div class="nh-searchButton">
								<input name="Submit" type="submit" class="buttons" onClick="return CheckData();" value=""/>
							</div>
							<div style="clear: both;"></div>
					    </div>
						<div style="clear: both;"></div>						
					</form>
					
				</div>
				
				<div style="clear: both;"></div>
				
			</div><!-- End of navigation -->
		
		<div><!-- End of containernavigation -->
