<% page_title = "Managing Page Content" %>
<!-- #include file="adm_header.asp" -->
<%
parent_id = request("pid")
if requestSafe(parent_id) = 1 then
	parent_id = 0
end if

if IsObject(conn) then
	conn.Close
end if

Set Connection = Server.CreateObject("ADODB.Connection")
Connection.Open strConn

banPerms = 0

SQL="SELECT count(*) as 'TotRecs' FROM siteuser_sitelevel WHERE user_id = " & session("user_id") & " AND section_id = 1"
set permRS = Connection.Execute(SQL)
do until permRS.eof
	TotRecs = permRS("TotRecs")
	permRS.Movenext
Loop
permRS.Close
set permRS = nothing

if TotRecs = 0 then
	banPerms = 1
end if

SQL="SELECT count(*) as 'TotRecs' FROM siteuser_sitelevel WHERE user_id = " & session("user_id") & " AND section_id = 1 and content_id = 0"
set permRS = Connection.Execute(SQL)
do until permRS.eof
	TotRecs = permRS("TotRecs")
	permRS.Movenext
Loop
permRS.Close
set permRS = nothing

if TotRecs = 1 then
	banPerms = 1
end if

if banPerms = 1 then
'	response.redirect "adm_menu.asp"
	response.write "<h4>You don't have permissions to edit this section</h4>"
end if

perm_i = -1
SQL="SELECT content_id FROM siteuser_sitelevel WHERE user_id = " & session("user_id") & " AND section_id = 1"
set permRS = Connection.Execute(SQL)
do until permRS.eof
	perm_i = perm_i + 1
	redim preserve permArr(perm_i)
	permArr(perm_i) = permRS("content_id")
	permRS.Movenext
Loop
permRS.Close

createPerms = 0
for perm_s = 0 to perm_i
	if CINT(permArr(perm_s)) = CINT(parent_id) then
		createPerms = 1
        
	end if
next

extraPerm_i = -1
SQLtext="SELECT DISTINCT section_id FROM siteuser_sitelevel WHERE user_id = " & session("user_id") & " AND section_id IN (2,3)"
set RecordSet = Connection.Execute(SQLtext)
do until RecordSet.eof
	extraPerm_i = extraPerm_i + 1
	redim preserve extraPermArr(extraPerm_i)
	extraPermArr(extraPerm_i) = RecordSet("section_id")
	RecordSet.Movenext
Loop
RecordSet.Close

showShuffleVar = "disabled "
showAddNewVar = 0
for extraPerm_s = 0 to extraPerm_i
	if CINT(extraPermArr(extraPerm_s)) = 2 then
		showShuffleVar = ""
	end if
	if CINT(extraPermArr(extraPerm_s)) = 3 then
		if createPerms = 1 then
			showAddNewVar = 1
		end if
	end if
next

contentSQL = "SELECT * FROM content WHERE parent_id = " & parent_id & " ORDER BY shuffle_order ASC"
content_s = -1
set RecordSet = Connection.Execute(contentSQL)
do until RecordSet.eof
	content_s = content_s + 1
	redim preserve contentArr(12,content_s)
	contentArr(0,content_s) = RecordSet("id")
	contentArr(1,content_s) = RecordSet("has_children")
   	contentArr(5,content_s) = RecordSet("script_name")
	contentArr(6,content_s) = RecordSet("name")
	contentArr(7,content_s) = RecordSet("shuffle_order")
	contentArr(8,content_s) = RecordSet("menu_item")
	contentArr(9,content_s) = RecordSet("edit_item")
	contentArr(10,content_s) = false
	contentArr(11,content_s) = RecordSet("preview_item")   
    contentArr(12,content_s) = RecordSet("members_page")
	for perm_s = 0 to perm_i
		if CINT(contentArr(0,content_s)) = CINT(permArr(perm_s)) then
			contentArr(10,content_s) = true
		end if
	next
	RecordSet.MoveNext
Loop
RecordSet.Close

if content_s = -1 then
	SQL="UPDATE content SET has_children = 0 WHERE id = " & parent_id
	Connection.Execute(SQL)
else
	SQL="UPDATE content SET has_children = 1 WHERE id = " & parent_id
	Connection.Execute(SQL)
end if
%>
<script language="JavaScript1.1" src="adm_content.js"></script>
<br/><br/>
<%
SELECT CASE session("AddColourSuccess")
	CASE 1
		response.write "<font color=#ff0000><b>content successfully added!</b></font>"
		session("AddColourSuccess") = null
	CASE 2
		response.write "<font color=#ff0000><b>content successfully edited!</b></font>"
		session("AddColourSuccess") = null
	CASE 3
		response.write "<font color=#ff0000><b>content successfully deleted!</b></font>"
		session("AddColourSuccess") = null
	CASE 4
		response.write "<font color=#ff0000><b>content Image successfully Uploaded!</b></font>"
		session("AddColourSuccess") = null
END SELECT
%>
<table width="100%" >
<% for content_l = 0 to content_s %>
<% if (contentArr(9,content_l)) = true then %>
<tr>
<th><strong><%= contentArr(6,content_l) %></strong></th>

<%
if (contentArr(10,content_l)) then
%>
	
	<td><%
		if (contentArr(9,content_l)) then
	%><a href="adm_content_edit.asp?id=<%= contentArr(0,content_l) %>">edit</a><%
		end if
	%></td>



<%
end if
%>
</tr>
<%
	finalShuffle = contentArr(7,content_l)
	end if
next
%>
</table>
<br />
<% if (showAddNewVar)  and parent_id <> 0 then %>

<% if IsNumeric(finalShuffle) then      
    finalShuffle = finalShuffle+1
    else
       finalShuffle = 0
   
end if 


 %>

<table id="no-border-table">
<form name="frmcontent" method="post" action="adm_content_addcode.asp">
<input type="hidden" name="parent_id" value="<%= parent_id %>" />
<input type="hidden" name="shuffle_order" value="<%= (finalShuffle) %>" />
<tr>
<td align="right"><b>Add new <%
	if parent_id <> 0 then
		%>sub-<%
	end if
%>content</b></td>

<td><input type="text" name="name" maxlength="50" size="40" /></td>
<td><input type="submit" name="Submit" value="Add Page" onClick="return checkcontentArr();" /></td>
</tr>
</form>
</table>

<% end if %>
<% if parent_id <> 0 then %>
<p><a href="adm_content_list.asp">return to main categories</a></p>
<% end if %>

<p><a href="menu.asp">return to main menu</a></p>


<!-- #include file="adm_footer.asp" -->