<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<!-- #include file="include_authenticate.asp" -->
<!--#include file="include_dbconnect.asp" -->
<%
	SQL="SELECT u.user_id, u.username, u.password, u.fullname FROM siteuser u ORDER BY u.username ASC"

user_s = -1

MM_ocnni_STRING = "Driver={SQL Server}; Server=" & SQL_Server & "; Database=" & SQL_Database & "; UID=" & SQL_UID & "; PWD=" & SQL_PWD

if NOT (IsObject(Connection)) then

	Dim Connection
	Set Connection = Server.CreateObject("ADODB.Connection")
	Connection.Open MM_ocnni_STRING

end if


Set RecordSet = Connection.Execute(SQL)
do while not RecordSet.EOF
	user_s = user_s + 1
	redim preserve userArr(3,user_s)
	userArr(0,user_s) = RecordSet("user_id")
	userArr(1,user_s) = RecordSet("username")
	userArr(2,user_s) = RecordSet("password")
	userArr(3,user_s) = RecordSet("fullname")
	RecordSet.movenext
loop
RecordSet.Close

RecsPerPage = 30
TotRecs = (user_s+1)
pageNo = request("page")
if requestSafe(pageNo) = 1 then
	pageNo = 1
end if
titleFlag = 0
if TotRecs > RecsPerPage AND (RecsPerPage-1) < user_s then
	startVar = ((pageNo*RecsPerPage)-RecsPerPage)
	endVar = ((pageNo*RecsPerPage)-1)
	if endVar > user_s then
		endVar = user_s
	end if
	titleFlag = 1
else
	startVar = 0
	endVar = user_s
	titleFlag = 0
end if
%>
<% page_title = "Aim Awards Users and Permissions" %>
<!--#include file="adm_header.asp" -->

<%
if session("seccpass") = "updated" then
	%><h1>Security settings for <%= session("seccpassfullname") %> successfully updated.</h1><%
end if
session("seccpass") = "0"
session("seccpassfullname") = null
%>



<%
if titleFlag = 1 then %>
<div id="nextpage" style="font-size: 10pt; text-align: center;"><%
	if pageNo > 1 then
		%><a href="?page=<%= (pageNo-1) %>" style="font-size: 10pt;">&laquo;&nbsp;previous</a><%
	else
		%>&laquo;&nbsp;previous<%
	end if
%><span style="font-size: 10pt;">&nbsp;&nbsp;Viewing users <%= (startVar+1) %> to <%= (endVar+1) %> of <%= TotRecs %>&nbsp;&nbsp;</span><%
	if endVar < user_s then
		%><a href="?page=<%= (pageNo+1) %>" style="font-size: 10pt;">next&nbsp;&raquo;</a><%
	else
		%>next&nbsp;&raquo;<%
	end if
%></div><%
end if
%>

<table border="0" align="center" cellpadding="0" cellspacing="0" width="100%">
<tr>
<th>&nbsp;</th>
<th><strong>Username<strong></th>
<!--<td>&nbsp;</td>
<th><strong>company</strong></th>
-->
<th><strong>Full name</strong></th>

<th><strong>Password</strong></th>
<th colspan="3">&nbsp;</th>
</tr>
<% for user_i = startVar to endVar %>
<tr>
<td><a href="#" onClick="return deleteCWUser('<%= userArr(0,user_i) %>');"><img src="images/delete.gif" border="0" /></a></td>
<td><%= userArr(3,user_i) %></td>

<td><%= userArr(1,user_i) %></td>

<td><%= userArr(2,user_i) %></td>

<td class="sec"><a href="sec_security_levels.asp?user_id=<%= userArr(0,user_i) %>"><strong>permissions</strong></a></td>
</tr>
<% next %>
</table>


<table>
<%
if session("NewUserCreate") = 1 then
	errDisplay = "<span class=""eventAlert"">Username already exists, please choose a new username</span>"
	session("NewUserCreate") = null
else
	errDisplay = null
	session("NewUserCreate") = null
end if
%>
<script language="JavaScript" type="text/JavaScript">
<!--
function isBlank(lstrText)
{
	for (var i = 0; i < lstrText.length; i++)
	{
		var chr = lstrText.charAt(i);
		if (chr != ' ') return false;
	}
	return true;
}

function echeck(emailAddress)
{
	str = emailAddress;
	varoutputStr = "";
	var at="@"
	var dot="."
	var lat=str.indexOf(at)
	var lstr=str.length
	var ldot=str.indexOf(dot)

	if (str.indexOf(at)==-1 || str.indexOf(at)==0 || str.indexOf(at)==lstr)
	{
		varoutputStr = "0";
	}

	if (str.indexOf(dot)==-1 || str.indexOf(dot)==0 || str.indexOf(dot)==lstr)
	{
		varoutputStr = "0";
	}

	if (str.indexOf(at,(lat+1))!=-1)
	{
		varoutputStr = "0";
	}

	if (str.substring(lat-1,lat)==dot || str.substring(lat+1,lat+2)==dot)
	{
		varoutputStr = "0";
	}

	if (str.indexOf(dot,(lat+2))==-1)
	{
		varoutputStr = "0";
	}

	if (str.indexOf(" ")!=-1)
	{
		varoutputStr = "0";
	}

	return varoutputStr;
}

function checkFields( querystring )
{
	ErrorCheckStr = "The following required fields are blank:\n\n";
	ErrorCheckVar = 0;

	if((document.form1.username.value == "")||(isBlank(document.form1.username.value)))
	{
		ErrorCheckVar = 1;
		ErrorCheckStr = ErrorCheckStr + "Email Address as Username\n";
	}
	else
	{
		if(echeck(document.form1.username.value) == "0")
		{
			ErrorCheckVar = 1;
			ErrorCheckStr = ErrorCheckStr + "You must use a real email address\n";
		}
		else
		{
			ErrorCheckVar = ErrorCheckVar;
		}
	}

	if(isBlank(document.form1.password.value))
	{
		ErrorCheckStr = ErrorCheckStr  + "Password\n";
		ErrorCheckVar = 1;
	}
	else
	{
		ErrorCheckVar = ErrorCheckVar;
	}

	if(isBlank(document.form1.fullname.value))
	{
		ErrorCheckStr = ErrorCheckStr  + "User's Full Name\n";
		ErrorCheckVar = 1;
	}
	else
	{
		ErrorCheckVar = ErrorCheckVar;
	}


	if(ErrorCheckVar > 0)
	{
		alert(ErrorCheckStr);
		return false;
	}
	else
	{
		return true;
	}
}
//-->
</script>
<form method="post" action="sec_securityAdd.asp" name="form1" onsubmit="return checkFields();">


<table border="0" cellpadding="0" cellspacing="0" class="boxer" id="no-border-table">
<tr><td colspan="2"><h3><strong>Add a new user</strong></h3></td></tr>
<tr valign="baseline">
<td nowrap align="right">Email:</td>
<td><input name="username" type="text" value="<%= session("NewUserCreate_username") %>" size="20" maxlength="30"></td>
</tr>
<tr valign="baseline">
<td nowrap align="right">Password:</td>
<td><input name="password" type="text" value="<%= session("NewUserCreate_password") %>" size="20" maxlength="20"></td>
</tr>
<tr valign="baseline">
<td nowrap align="right">Full name:</td>
<td><input name="fullname" type="text" value="<%= session("NewUserCreate_fullname") %>" size="20" maxlength="50"> </td>
</tr>
<!--
<tr valign="baseline">
<td>&nbsp;</td>
<td><input name="uploadFlag" type="checkbox" value="1" />:Allow Uploads</td>
</tr>
<tr valign="baseline">
<td>&nbsp;</td>
<td><input name="commentFlag" type="checkbox" value="1" />:Allow Comments</td>
</tr>
-->
<tr valign="baseline">
<td nowrap align="right">&nbsp;</td>
<td><input name="submit" type="submit" value="Add New User" class="button_text2"></td>
</tr>
</table> 
<input type="hidden" name="MM_insert" value="form1">
</form>
<p><%= errDisplay %>&nbsp;</p>
</td>
</tr>
</table>  
<%
if session("cms_user_id") = 1 then
	%><div align="center"><a href="sec_levels.asp">view/edit security levels</a></div><%
end if
%>
<!-- #include file="adm_footer.asp" -->