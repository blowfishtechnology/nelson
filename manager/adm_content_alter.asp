<!-- #include file="include_dbconnect.asp" -->
<!-- #include file="ocnser.asp" -->
<%
type_id = request.querystring("tid")
if requestSafe(type_id) = 1 then
	type_id = 0
end if
content_id = request.querystring("pid")
if requestSafe(content_id) = 1 then
	content_id = 0
end if

Dim conn
call openConn()

SQL="SELECT parent_id, menu_item, preview_item, edit_item,members_page FROM content WHERE id = " & content_id
set RecordSet = conn.Execute(SQL)
do until RecordSet.eof
	parent_id = RecordSet("parent_id")
	menu_item = RecordSet("menu_item")
	edit_item = RecordSet("edit_item")
	preview_item = recordset("Preview_item")
    members_page = recordset("members_page")
	RecordSet.MoveNext
Loop
RecordSet.Close

' type_id
' 0 = menu_item
' 1 = edit_item
' 2 = preview_item
' 3 = members_page
SELECT CASE type_id
	case "0"
		if (menu_item) then
			SQL="UPDATE content SET menu_item = 0 WHERE id = " & content_id
		else
			SQL="UPDATE content SET menu_item = 1 WHERE id = " & content_id
		end if
		conn.Execute(SQL)
	case "1"
		if (edit_item) then
			SQL="UPDATE content SET edit_item = 0 WHERE id = " & content_id
		else
			SQL="UPDATE content SET edit_item = 1 WHERE id = " & content_id
		end if
		conn.Execute(SQL)
	case "2"
		if (preview_item) then
			SQL="UPDATE content SET preview_item = 0 WHERE id = " & content_id
		else
			SQL="UPDATE content SET preview_item = 1 WHERE id = " & content_id
		end if

    case "3"
		if (members_page) then
			SQL="UPDATE content SET members_page = 0 WHERE id = " & content_id
		else
			SQL="UPDATE content SET members_page = 1 WHERE id = " & content_id
		end if
		conn.Execute(SQL)
END SELECT

reDirStr = "adm_content_list.asp?pid=" & parent_id
response.redirect reDirStr
%>