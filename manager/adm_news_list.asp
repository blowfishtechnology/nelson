<% page_title = "Full News Article List" %>
<!-- #include file="adm_header.asp" -->
<%
sNews = -1
SQL="SELECT news_id, news_title, news_date, news_summary, news_type_id " &_
	"FROM news " &_
	"WHERE archiveFlag IS NULL OR archiveFlag = 0 " &_
	"AND news_title <> '' " &_
	"ORDER BY news_date DESC"
set recordset = conn.execute(SQL)
do while not recordset.eof
	sNews = sNews + 1
	redim preserve newsArr(5,sNews)
	newsArr(0,sNews) = recordset("news_id")
	newsArr(1,sNews) = recordset("news_title")
	newsArr(2,sNews) = recordset("news_date")
	newsArr(3,sNews) = recordset("news_summary")
	newsArr(4,sNews) = recordset("news_type_id")
	recordset.movenext
loop
recordset.close
%>


<a href="adm_news_page.asp?id=new" class="button_text2">Add a news article</a>



<a href="adm_news_archive_list.asp" class="button_text2">view archived news</a>

<div class="surroundgreydark">
<h5>Viewing Live News </h5>
<table cellspacing="0" cellpadding="0" width="100%">
<th>News Article Title</th>
<th>Date Posted</th>
<th>Delete</th>
<th>Archive</th>
<%
for iNews = 0 to sNews
	if (iNews mod 2) = 0 then
		trackerStr = " style=""background-color: #CCCCCC;"""
	else
		trackerStr = null
	end if
%>

<tr<%= trackerStr %>>
<td><a href="adm_news_page.asp?id=<%= newsArr(0,iNews) %>"><%= newsArr(1,iNews) %></a></td>
<td><%= kaos_date("%D/%M/%y",newsArr(2,iNews)) %></td>
<td><a href="#" onClick="return deleteNews('<%= newsArr(0,iNews) %>');" class="delete">delete</a></td>

<td><%

%><a href="adm_news_archive.asp?id=<%= newsArr(0,iNews) %>">archive this</a></td>
</tr>


<!--<tr>
<td colspan="5"><%= newsArr(3,iNews) %></td>
</tr>-->
<% next %>
</table>
</div>
<!-- #include file="adm_footer.asp" -->