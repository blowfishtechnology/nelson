<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<!-- #include file="include_authenticate.asp" -->
<!-- #include file="ocnser.asp" -->
<!-- #include file="date_function.asp" -->
<%
actionStr = Request("action")

If actionStr = "GO" Then

	subject_category_Original = request("subject_category_Original")
	subject_category = request("subject_category")

	if len(subject_category) > 1 then
		subject_category = trim(subject_category)
		subject_category = sqlSafe(subject_category)

		q_id_s = -1
		SQL="SELECT q_id FROM qualification WHERE subject_category = '" & subject_category_Original & "'"
		Set RecordSet = Connection.Execute(SQL)
		do while not RecordSet.EOF
			q_id_s = q_id_s + 1
			redim preserve q_idArr(q_id_s)
			q_idArr(q_id_s) = RecordSet("q_id")
			RecordSet.MoveNext
		loop
		RecordSet.Close

		if q_id_s > -1 then
			q_idList = join(q_idArr,",")
		else
			q_idList = 0
		end if

		SQL="UPDATE qualification SET subject_category = '" & subject_category & "' " &_
			"WHERE q_id IN (" & q_idList & ")"
		Connection.Execute(SQL)

	end if

End If

sec_s = -1
SQL="SELECT subject_category, count(subject_category) as 'TotRecs'  FROM qualification GROUP BY subject_category ORDER BY subject_category"
Set RecordSet = Connection.Execute(SQL)
do while not RecordSet.EOF
	sec_s = sec_s + 1
	redim preserve secArr(1,sec_s)
	secArr(0,sec_s) = RecordSet("subject_category")
	secArr(1,sec_s) = RecordSet("TotRecs")
	RecordSet.MoveNext
loop
RecordSet.Close
%>
<!-- #include file="adm_header.asp" -->

<h1>Viewing/Editing Categories</h1>

<h2>Subject Categories</h2>

<style>
.supportframebg {
	color: red;
	background: #cccccc;
	width: 700px;
	clear: both;
	height: 26px;
	padding: 2px;
}

.supportframe {
	width: 700px;
	clear: both;
	height: 26px;
	padding: 2px;
}

.titlespace {
	padding: 4px 0 0 0;
	text-indent: 4px;
	float: left;
	width: 350px;
}
.formspace {
	float: right;
}
.formspace .subject_category {
	width: 275px;
}

</style>

<div>

<% for iSec = 0 to sec_s %>
<div class="<%
if (iSec mod 2) = 0 then
	%>supportframebg<%
else
	%>supportframe<%
end if
%>">
	<form method="post" action="?action=GO" name="form<%= iSec %>" id="form<%= iSec %>">
		<input type="hidden" name="subject_category_Original" value="<%= secArr(0,iSec) %>" />
		<div class="formspace">
			<input type="text" name="subject_category" class="subject_category" value="<%= secArr(0,iSec) %>" />
			<input type="submit" value="Update" />
		</div>
		<div class="titlespace"><%= secArr(0,iSec) %> (<%= secArr(1,iSec) %>)</div>
	</form>
</div>
<% next %>

</div>
<!-- #include file="adm_footer.asp" -->