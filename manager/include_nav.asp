<style type="text/css">
<!--
.styleLink {color: #FFFFFF}
-->
</style>
<table width="100%"  border="0" cellspacing="4" cellpadding="0">
  <tr>
    <td><a href="credits.asp" class="styleLink">Credits &amp; Levels </a></td>
  </tr>
  <tr>
    <td height="1" bgcolor="#4D1985"></td>
  </tr>
  <tr>
    <td><a href="products.asp">Products &amp; Services</a> </td>
  </tr>
  <tr>
    <td height="1" bgcolor="#4D1985"></td>
  </tr>
  <tr>
    <td><a href="fees.asp">Fees Schedule </a></td>
  </tr>
  <tr>
    <td height="1" bgcolor="#4D1985"></td>
  </tr>
  <tr>
    <td><a href="registering.asp">Registering Courses &amp; Learners </a></td>
  </tr>
  <tr>
    <td height="1" bgcolor="#4D1985"></td>
  </tr>
  <tr>
    <td><a href="quality.asp">Quality Assurance </a></td>
  </tr>
  <tr>
    <td height="1" bgcolor="#4D1985"></td>
  </tr>
  <tr>
    <td><a href="standevents.asp">Standardisation Events</a></td>
  </tr>
  <tr>
    <td height="1" bgcolor="#4D1985"></td>
  </tr>
  <tr>
    <td><a href="standeventstext.asp">Standardisation Events Text</a></td>
  </tr>
  <tr>
    <td height="1" bgcolor="#4D1985"></td>
  </tr>
  <tr>
    <td><a href="calendar.asp">Training Calendar</a></td>
  </tr>
  <tr>
    <td height="1" bgcolor="#4D1985"></td>
  </tr>
  <tr>
    <td><a href="testimonials.asp">Testimonials</a> | <a href="testimonials_new.asp">Add New</a></td>
  </tr>
  <tr>
    <td height="1" bgcolor="#4D1985"></td>
  </tr>
  <tr>
    <td><a href="aboutus.asp">About Us</a> </td>
  </tr>
  <tr>
    <td height="1" bgcolor="#4D1985"></td>
  </tr>
  <tr>
    <td><a href="contactus.asp">Contact Us</a> | <a href="staff.asp">Staff</a></td>
  </tr>
  <tr>
    <td height="1" bgcolor="#4D1985"></td>
  </tr>
  <tr>
    <td><a href="links.asp">Family Learning Project</a></td>
  </tr>
  <tr>
    <td height="1" bgcolor="#4D1985"></td>
  </tr>
  <tr>
    <td><a href="nqf.asp">Qualifications/Units within the NQF</a></td>
  </tr>
  <tr>
    <td height="1" bgcolor="#4D1985"></td>
  </tr>
  <tr>
    <td><a href="appcentre.asp">How to become an approved centre</a></td>
  </tr>
  <tr>
    <td height="1" bgcolor="#4D1985"></td>
  </tr>
  <tr>
    <td><a href="adm_news_list.asp">OCNNI News Area</a></td>
  </tr>
  <tr>
    <td height="1" bgcolor="#4D1985"></td>
  </tr>
  <tr>
    <td><a href="adm_upload.asp">Uploaded Documents</a></td>
  </tr>
  <tr>
    <td style="text-align: right;">&nbsp;|&nbsp;<a href="adm_gallery.asp">Document Sections</a></td>
  </tr>
  <tr>
    <td height="1" bgcolor="#4D1985"></td>
  </tr>
  <tr>
    <td><a href="faq.asp">Frequently Asked Questions</a></td>
  </tr>
  <tr>
    <td height="1" bgcolor="#4D1985"></td>
  </tr>
  <tr>
    <td><a href="jobs.asp">Latest Vacancies</a></td>
  </tr>
</table>