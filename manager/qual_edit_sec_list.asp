<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<!-- #include file="include_authenticate.asp" -->
<!-- #include file="ocnser.asp" -->
<!-- #include file="date_function.asp" -->
<%
actionStr = Request("action")

If actionStr = "GO" Then

	sub_sector_Original = request("sub_sector_Original")
	sub_sector = request("sub_sector")

	if len(sub_sector) > 1 then
		sub_sector = trim(sub_sector)
		sub_sector = sqlSafe(sub_sector)

		q_id_s = -1
		SQL="SELECT q_id FROM qualification WHERE sub_sector = '" & sub_sector_Original & "'"
		Set RecordSet = Connection.Execute(SQL)
		do while not RecordSet.EOF
			q_id_s = q_id_s + 1
			redim preserve q_idArr(q_id_s)
			q_idArr(q_id_s) = RecordSet("q_id")
			RecordSet.MoveNext
		loop
		RecordSet.Close

		if q_id_s > -1 then
			q_idList = join(q_idArr,",")
		else
			q_idList = 0
		end if

		SQL="UPDATE qualification SET sub_sector = '" & sub_sector & "' " &_
			"WHERE q_id IN (" & q_idList & ")"
		Connection.Execute(SQL)

	end if

End If

sec_s = -1
SQL="SELECT sub_sector, count(sub_sector) as 'TotRecs'  FROM qualification GROUP BY sub_sector ORDER BY sub_sector"
Set RecordSet = Connection.Execute(SQL)
do while not RecordSet.EOF
	sec_s = sec_s + 1
	redim preserve secArr(1,sec_s)
	secArr(0,sec_s) = RecordSet("sub_sector")
	secArr(1,sec_s) = RecordSet("TotRecs")
	RecordSet.MoveNext
loop
RecordSet.Close
%>
<!-- #include file="adm_header.asp" -->

<h1>Viewing/Editing Sectors</h1>

<h2>Subject Sectors</h2>

<style>
.supportframebg {
	color: red;
	background: #cccccc;
	width: 700px;
	clear: both;
	height: 26px;
	padding: 2px;
}

.supportframe {
	width: 700px;
	clear: both;
	height: 26px;
	padding: 2px;
}

.titlespace {
	padding: 4px 0 0 0;
	text-indent: 4px;
	float: left;
	width: 350px;
}
.formspace {
	float: right;
}
.formspace .sub_sector {
	width: 275px;
}

</style>

<div>

<% for iSec = 0 to sec_s %>
<div class="<%
if (iSec mod 2) = 0 then
	%>supportframebg<%
else
	%>supportframe<%
end if
%>">
	<form method="post" action="?action=GO" name="form<%= iSec %>" id="form<%= iSec %>">
		<input type="hidden" name="sub_sector_Original" value="<%= secArr(0,iSec) %>" />
		<div class="formspace">
			<input type="text" name="sub_sector" class="sub_sector" value="<%= secArr(0,iSec) %>" />
			<input type="submit" value="Update" />
		</div>
		<div class="titlespace"><%= secArr(0,iSec) %> (<%= secArr(1,iSec) %>)</div>
	</form>
</div>
<% next %>

</div>
<!-- #include file="adm_footer.asp" -->