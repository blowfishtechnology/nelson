﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="OCNSERLogin.Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<title>Login | OCNSER Content Management System</title>
<link href="css/manager.css" rel="stylesheet" type="text/css" />
<link href="css/adminstyle.css" rel="stylesheet" type="text/css" />
</head>
<body>
    
<div id="wrapper">
	<div id="header2">

</div>
<!-- end #header -->
<div id="content-surround-login" >

<div id="main-nav">

</div>
<div id="primary-content-login">
<h2 class="page-title">OCNSER CMS Login</h2>    <form id="form1" runat="server" class="loginform roundify">        Username:<br />        <asp:TextBox runat="server" ID="username"></asp:TextBox>        <br />        <br />        Password:<br />        <asp:TextBox runat="server" ID="password" TextMode="Password"></asp:TextBox>        <br />        <br />        <asp:Button runat="server" ID="submit" CssClass="submit roundify2" OnClick="btnLogin_Click" Text="Login" />        <asp:Label Visible="false" runat="server" ID="txtMessages" ForeColor="red"></asp:Label>        </form>
    
    
</div><!-- end #mainContent -->
</div>
<div id="footer"> 


<!--begin copyright-->
	<div id="copyright">
	<p>&copy; Copyright Web Solutions NI Ltd 2010. All rights reserved.</p>

		</div><!--end copyright-->


    <!--begin colophon-->
	<div id="colophon">

		<a href="http://www.web-solutions-ni.com" class="web-solutions-logo-button">Site designed and hosted by<br />Web Solutions NI</a>

		</div> <!--end colophon-->






<!-- end #footer --></div>
<!-- end #wrapper --></div>
</body>
</html>
