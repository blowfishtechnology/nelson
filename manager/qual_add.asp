<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<!-- #include file="include_authenticate.asp" -->
<!-- #include file="ocnser.asp" -->
<!-- #include file="date_function.asp" -->
<%
actionStr = Request("action")

'Dim Connection
'Set Connection = Server.CreateObject("ADODB.Connection")
'Connection.Open MM_ocnni_STRING

If actionStr = "GO" Then

	Set Upload = Server.CreateObject("Persits.Upload.1")
	Upload.OverwriteFiles = False
	Count = Upload.SaveToMemory
	PathFolder = Server.MapPath("../quals")

	filename = ""

	For Each File in Upload.Files
		Path = PathFolder & "\" & File.ExtractFileName
		File.SaveAs Path
		filename = File.ExtractFileName
	next

	title = upload.form("title") ' = sdf
	description = upload.form("description") ' = dfgh
    
	SQL="SET nocount ON; INSERT INTO qualification (title,description,filename ) VALUES (" &_
		"'" & sqlSafe(title) & "', " &_	
        		"'" & sqlSafe(description) & "'," &_                 
			 "'" & sqlSafe(filename) & "')"
	Set RecordSet = Connection.Execute(SQL)    
    	

	response.redirect "qual_list.asp"
End If

%>
<!-- #include file="adm_header.asp" -->



<h2 class="page-title">Products</h2>
<br />
<div style="padding: 5px; width: 500px; text-align:center;" class="style3">Adding/Editing a Product</div>
<br />

<table>

<form method="post" action="?action=GO" name="form1" ENCTYPE="multipart/form-data">

<tr valign="baseline">
<td nowrap align="right">Product Name:</td>
<td><input type="text" name="title" value="<%= title %>" size="50" /></td>
</tr>

<tr valign="baseline">
<td nowrap align="right" valign="top">Description:</td>
<td>

<%
Dim oFCKeditor
Set oFCKeditor = New FCKeditor
oFCKeditor.BasePath = ""
oFCKeditor.Value = description
oFCKeditor.height = 250
oFCKeditor.width = 400
oFCKeditor.ToolbarSet = "Basic"
oFCKeditor.Create "description"
%>
</td>
</tr>

<tr valign="baseline">
<td nowrap align="right">Upload a file:</td>
<td><input type="file" name="filename" size="50" /> <%= filename %></td>
</tr>

<tr valign="baseline">
<td nowrap align="right">&nbsp;</td>
<td><input type="submit" value="Update This Product"></td>
</tr>
</table>
</form>

<!-- #include file="adm_footer.asp" -->