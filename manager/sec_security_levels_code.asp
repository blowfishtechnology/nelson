<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<!--#include file="adm_header.asp" -->
<!-- #include file="include_authenticate.asp" -->
<!--#include file="connect.asp" -->
<%
user_id = request.form("user_id") ' = 1
uploadFlag = request.form("uploadFlag")
commentFlag = request.form("commentFlag")
level_id = request.form("level_id") ' = 1, 2, 8, 9, 10

session("seccpass") = "0"

MM_ocnni_STRING = "Driver={SQL Server}; Server=" & SQL_Server & "; Database=" & SQL_Database & "; UID=" & SQL_UID & "; PWD=" & SQL_PWD

if NOT (IsObject(Connection)) then

	Dim Connection
	Set Connection = Server.CreateObject("ADODB.Connection")
	Connection.Open MM_ocnni_STRING

end if

if requestSafe(user_id) = 0 then

	SQLtext="SELECT fullname FROM siteuser WHERE user_id = " & user_id
	set userRS = Connection.Execute(SQLtext)
	do until userRS.eof
		fullname = userRS("fullname")
		userRS.Movenext
	Loop
	userRS.Close

	levelArr = split(level_id,", ")

	if uBound(levelArr) > -1 then
		SQLdelete="DELETE FROM siteuser_sitelevel WHERE user_id = " & user_id
		Connection.Execute(SQLdelete)

		for i = 0 to uBound(levelArr)
			SQLtext="INSERT INTO siteuser_sitelevel (user_id,section_id,content_id) VALUES (" & user_id & "," & levelArr(i) & ",'1')"
			Connection.Execute(SQLtext)
		next
	end if

	if requestSafe(uploadFlag) then
		uploadFlag = 0
	end if
	if requestSafe(commentFlag) then
		commentFlag = 0
	end if

	SQL="UPDATE siteuser SET uploadFlag = " & uploadFlag & ", " &_
		"commentFlag = " & commentFlag & " " &_
		"WHERE user_id = " & user_id
	Connection.Execute(SQL)
	Connection.Close

	session("seccpassfullname") = fullname
	session("seccpass") = "updated"

end if

response.redirect "sec_security.asp"
%>