<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<!--#include file="adm_header.asp" -->
<!-- #include file="include_authenticate.asp" -->
<!--#include file="connect.asp" -->
<%
MM_ocnni_STRING = "Driver={SQL Server}; Server=" & SQL_Server & "; Database=" & SQL_Database & "; UID=" & SQL_UID & "; PWD=" & SQL_PWD

if NOT (IsObject(Connection)) then

	Dim Connection
	Set Connection = Server.CreateObject("ADODB.Connection")
	Connection.Open MM_ocnni_STRING

end if

user_id = Request.QueryString("user_id")
if requestSafe(user_id) = 1 then
	user_id = 0
end if

perm_i = -1
SQL="SELECT DISTINCT section_id FROM siteuser_sitelevel WHERE user_id = " & user_id
set permRS = Connection.Execute(SQL)
do until permRS.eof
	perm_i = perm_i + 1
	redim preserve permArr(perm_i)
	permArr(perm_i) = permRS("section_id")
	permRS.Movenext
Loop
permRS.Close

level_i = -1
SQL="SELECT * FROM [sitelevel] ORDER BY [level_id]"
set levelsRS = Connection.Execute(SQL)
do until levelsRS.eof
	level_i = level_i + 1
	redim preserve levelArr(2,level_i)
	levelArr(0,level_i) = levelsRS("level_id")
	levelArr(1,level_i) = levelsRS("level")
	levelArr(2,level_i) = levelsRS("hasChildren")
	levelsRS.Movenext
Loop
levelsRS.Close

SQLtext="SELECT * FROM siteuser WHERE user_id = " & user_id
set userRS = Connection.Execute(SQLtext)
do until userRS.eof
	username = userRS("username")
	uploadFlag = userRS("uploadFlag")
	commentFlag = userRS("commentFlag")
	userRS.Movenext
Loop
userRS.Close

PageNameStr = "Editing Permissions for " & username
%>


<br />
<h2 class="page-title">Users and Permissions </h2>

<form method="post" action="sec_security_levels_code.asp" name="form1">
<input type="hidden" name="user_id" value="<%= user_id %>">


<!--
<td valign="top">

<table cellpadding="0" cellspacing="0" class="no-border-table" width="100%">
<tr valign="baseline">
<td align="center"><strong>Browsing</strong></td>
</tr>
<tr valign="baseline">
<td><input type="checkbox" name="uploadFlag" value="1"<%
	if (uploadFlag) then
		response.write " checked"
	end if
%>> Allow Uploads</td>
</tr>
<tr valign="baseline">
<td><input type="checkbox" name="commentFlag" value="1"<%
	if (commentFlag) then
		response.write " checked"
	end if
%>> Allow Comments</td>
</tr>
</table>
</td>
-->

<h5>Sections<%
SELECT CASE session("secpermedit")
	CASE 1
		%>&nbsp;&nbsp;-&nbsp;&nbsp;<font color="#ff0000">Permissions updated!</font>&nbsp;&nbsp;
		<a href="sec_security.asp?user_id=<%= user_id %>">back to security user list</a><%
	CASE 2
		%>&nbsp;&nbsp;-&nbsp;&nbsp;<font color="#ff0000">Permissions NOT updated!</font>&nbsp;&nbsp;
		<a href="sec_security.asp?user_id=<%= user_id %>">back to security user list</a><%
END SELECT
session("secpermedit") = null
%></h5>
<table cellpadding="0" cellspacing="0" width="100%" >
<%
for level_s = 0 to level_i
	chkStr = null
	for perm_s = 0 to perm_i
		if CINT(levelArr(0,level_s)) = CINT(permArr(perm_s)) then
			chkStr = " checked"
		end if
	next
	%>
	<tr valign="baseline">
	<td><input type="checkbox" name="level_id" value="<%= levelArr(0,level_s) %>"<%= chkStr %> /></td>
	<th><%= levelArr(1,level_s) %></th>
	<td><%
		if (levelArr(2,level_s)) then
			%><a href="sec_security_levels_detail.asp?id=<%= levelArr(0,level_s) %>&user_id=<%= user_id %>">Detail</a><%
		else
			%>&nbsp;<%
		end if
		%></td>
	</tr>
	<%
next
%>

</table>

<input type="submit" name="submit" value="Update Security" class="button_text2"/>

</form>
<p>&nbsp;</p>
<!-- #include file="adm_footer.asp" -->