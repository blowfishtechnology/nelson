<% page_title = "Editing a project" %>
<!-- #include file="adm_header.asp" -->
<%
news_id = request("id")
if requestSafe(news_id) = 1 and news_id <> "new" then
	response.redirect "adm_event_list.asp"
end if

if news_id = "new" then
	news_type_id = 0
	page_title = "Adding a project"
%>

<div>
<form name="form1" method="post" action="adm_project_page_add.asp" enctype="multipart/form-data">
<input type="hidden" name="news_type_id" value="1">
<%
else

	conSQL="SELECT * FROM projects WHERE news_id = " & news_id
	set recordset = conn.execute(conSQL)
	do while not recordset.eof
		news_id = recordset("news_id")
		news_title = recordset("news_title")
		news_date = recordset("news_date")
		news_summary = recordset("news_summary")
		news_article = recordset("news_article")
		news_image = recordset("news_image")
		news_type_id = recordset("news_type_id")
        accesstohe = recordset("accesstohe")
		recordset.movenext
	loop
	recordset.close

	news_day = kaos_date("%D", news_date)
	news_month = kaos_date("%M", news_date)
	news_year = kaos_date("%Y", news_date)

	if news_type_id = "1" then
		articleTypeNews = " selected"
	elseif news_type_id = "2" then
		articleTypeEvent = " selected"
	end if
%>

<strong><%= content_title %></strong>
<br />

<div>
<form name="form1" method="post" action="adm_project_page_edit.asp" enctype="multipart/form-data">
<input type="hidden" name="news_id" value="<%= news_id %>">
<input type="hidden" name="news_type_id" value="1">
<%
end if
%>
<p><strong>Title:</strong></p>
<p><input name="news_title" type="text" size="80" value="<%= news_title %>" /></p>


<input name="news_summary" type="hidden" size="80" value="<%= news_image %>" />

<p></p>

<strong>Content:</strong>
<p><%
Dim oFCKeditor1
Set oFCKeditor1 = New FCKeditor
oFCKeditor1.BasePath = ""
oFCKeditor1.width = "620px"
oFCKeditor1.Height = 300
oFCKeditor1.Value = news_article
oFCKeditor1.ToolbarSet = "CustomToolbarNoFont"
oFCKeditor1.Create "news_article"
%></p>

<td colspan="2">  <P></P>
           
           <input type="radio" value="1" name="AccesstoHE" id="AccesstoHE" <% if (accesstohe = 1) then %>checked<% end if %>/>
           Current
          <!--- <input type="radio" value="2" name="AccesstoHE" id="AccesstoHE" <% if (accesstohe = 2) then %>checked<% end if %>/>
           Previous --->
            <input type="radio" value="3" name="AccesstoHE" id="AccesstoHE" <% if (accesstohe = 3) then %>checked<% end if %>/>
           &nbsp; Case Study
           <P></P>
          </td>

	<% if news_id = "new" then %>
<p><input name="submit" type="submit" value="Save this project"></p>
<% else %>
<p><input name="submit" type="submit" value="Save these changes"></p>
<% end if %>
</form>
</div>

<!-- #include file="adm_footer.asp" -->