<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<!-- #include file="include_authenticate.asp" -->
<!-- #include file="ocnser.asp" -->
<!-- #include file="date_function.asp" -->
<%
level_s = -1

	SQL="SELECT * FROM qualification "

	
%>
<%
Dim cqfRS
Dim cqfRS_numRows

Set cqfRS = Server.CreateObject("ADODB.Recordset")
cqfRS.ActiveConnection = MM_ocnni_STRING
cqfRS.Source = SQL
cqfRS.CursorType = 0
cqfRS.CursorLocation = 2
cqfRS.LockType = 1
cqfRS.Open()

cqfRS_numRows = 0
%>
<%
Dim Repeat1__numRows
Dim Repeat1__index

Repeat1__numRows = 20
Repeat1__index = 0
cqfRS_numRows = cqfRS_numRows + Repeat1__numRows
%>
<%
'  *** Recordset Stats, Move To Record, and Go To Record: declare stats variables

Dim cqfRS_total
Dim cqfRS_first
Dim cqfRS_last

' set the record count
cqfRS_total = cqfRS.RecordCount

' set the number of rows displayed on this page
If (cqfRS_numRows < 0) Then
  cqfRS_numRows = cqfRS_total
Elseif (cqfRS_numRows = 0) Then
  cqfRS_numRows = 1
End If

' set the first and last displayed record
cqfRS_first = 1
cqfRS_last  = cqfRS_first + cqfRS_numRows - 1

' if we have the correct record count, check the other stats
If (cqfRS_total <> -1) Then
  If (cqfRS_first > cqfRS_total) Then
    cqfRS_first = cqfRS_total
  End If
  If (cqfRS_last > cqfRS_total) Then
    cqfRS_last = cqfRS_total
  End If
  If (cqfRS_numRows > cqfRS_total) Then
    cqfRS_numRows = cqfRS_total
  End If
End If
%>
<%
' *** Recordset Stats: if we don't know the record count, manually count them

If (cqfRS_total = -1) Then

  ' count the total records by iterating through the recordset
  cqfRS_total=0
  While (Not cqfRS.EOF)
    cqfRS_total = cqfRS_total + 1
    cqfRS.MoveNext
  Wend

  ' reset the cursor to the beginning
  If (cqfRS.CursorType > 0) Then
    cqfRS.MoveFirst
  Else
    cqfRS.Requery
  End If

  ' set the number of rows displayed on this page
  If (cqfRS_numRows < 0 Or cqfRS_numRows > cqfRS_total) Then
    cqfRS_numRows = cqfRS_total
  End If

  ' set the first and last displayed record
  cqfRS_first = 1
  cqfRS_last = cqfRS_first + cqfRS_numRows - 1

  If (cqfRS_first > cqfRS_total) Then
    cqfRS_first = cqfRS_total
  End If
  If (cqfRS_last > cqfRS_total) Then
    cqfRS_last = cqfRS_total
  End If

End If
%>
<%
Dim MM_paramName
%>
<%
' *** Move To Record and Go To Record: declare variables

Dim MM_rs
Dim MM_rsCount
Dim MM_size
Dim MM_uniqueCol
Dim MM_offset
Dim MM_atTotal
Dim MM_paramIsDefined

Dim MM_param
Dim MM_index

Set MM_rs    = cqfRS
MM_rsCount   = cqfRS_total
MM_size      = cqfRS_numRows
MM_uniqueCol = ""
MM_paramName = ""
MM_offset = 0
MM_atTotal = false
MM_paramIsDefined = false
If (MM_paramName <> "") Then
  MM_paramIsDefined = (Request.QueryString(MM_paramName) <> "")
End If
%>
<%
' *** Move To Record: handle 'index' or 'offset' parameter

if (Not MM_paramIsDefined And MM_rsCount <> 0) then

  ' use index parameter if defined, otherwise use offset parameter
  MM_param = Request.QueryString("index")
  If (MM_param = "") Then
    MM_param = Request.QueryString("offset")
  End If
  If (MM_param <> "") Then
    MM_offset = Int(MM_param)
  End If

  ' if we have a record count, check if we are past the end of the recordset
  If (MM_rsCount <> -1) Then
    If (MM_offset >= MM_rsCount Or MM_offset = -1) Then  ' past end or move last
      If ((MM_rsCount Mod MM_size) > 0) Then         ' last page not a full repeat region
        MM_offset = MM_rsCount - (MM_rsCount Mod MM_size)
      Else
        MM_offset = MM_rsCount - MM_size
      End If
    End If
  End If

  ' move the cursor to the selected record
  MM_index = 0
  While ((Not MM_rs.EOF) And (MM_index < MM_offset Or MM_offset = -1))
    MM_rs.MoveNext
    MM_index = MM_index + 1
  Wend
  If (MM_rs.EOF) Then
    MM_offset = MM_index  ' set MM_offset to the last possible record
  End If

End If
%>
<%
' *** Move To Record: if we dont know the record count, check the display range

If (MM_rsCount = -1) Then

  ' walk to the end of the display range for this page
  MM_index = MM_offset
  While (Not MM_rs.EOF And (MM_size < 0 Or MM_index < MM_offset + MM_size))
    MM_rs.MoveNext
    MM_index = MM_index + 1
  Wend

  ' if we walked off the end of the recordset, set MM_rsCount and MM_size
  If (MM_rs.EOF) Then
    MM_rsCount = MM_index
    If (MM_size < 0 Or MM_size > MM_rsCount) Then
      MM_size = MM_rsCount
    End If
  End If

  ' if we walked off the end, set the offset based on page size
  If (MM_rs.EOF And Not MM_paramIsDefined) Then
    If (MM_offset > MM_rsCount - MM_size Or MM_offset = -1) Then
      If ((MM_rsCount Mod MM_size) > 0) Then
        MM_offset = MM_rsCount - (MM_rsCount Mod MM_size)
      Else
        MM_offset = MM_rsCount - MM_size
      End If
    End If
  End If

  ' reset the cursor to the beginning
  If (MM_rs.CursorType > 0) Then
    MM_rs.MoveFirst
  Else
    MM_rs.Requery
  End If

  ' move the cursor to the selected record
  MM_index = 0
  While (Not MM_rs.EOF And MM_index < MM_offset)
    MM_rs.MoveNext
    MM_index = MM_index + 1
  Wend
End If
%>
<%
' *** Move To Record: update recordset stats

' set the first and last displayed record
cqfRS_first = MM_offset + 1
cqfRS_last  = MM_offset + MM_size

If (MM_rsCount <> -1) Then
  If (cqfRS_first > MM_rsCount) Then
    cqfRS_first = MM_rsCount
  End If
  If (cqfRS_last > MM_rsCount) Then
    cqfRS_last = MM_rsCount
  End If
End If

' set the boolean used by hide region to check if we are on the last record
MM_atTotal = (MM_rsCount <> -1 And MM_offset + MM_size >= MM_rsCount)
%>
<%
' *** Go To Record and Move To Record: create strings for maintaining URL and Form parameters

Dim MM_keepNone
Dim MM_keepURL
Dim MM_keepForm
Dim MM_keepBoth

Dim MM_removeList
Dim MM_item
Dim MM_nextItem

' create the list of parameters which should not be maintained
MM_removeList = "&index="
If (MM_paramName <> "") Then
  MM_removeList = MM_removeList & "&" & MM_paramName & "="
End If

MM_keepURL=""
MM_keepForm=""
MM_keepBoth=""
MM_keepNone=""

' add the URL parameters to the MM_keepURL string
For Each MM_item In Request.QueryString
  MM_nextItem = "&" & MM_item & "="
  If (InStr(1,MM_removeList,MM_nextItem,1) = 0) Then
    MM_keepURL = MM_keepURL & MM_nextItem & Server.URLencode(Request.QueryString(MM_item))
  End If
Next

' add the Form variables to the MM_keepForm string
For Each MM_item In Request.Form
  MM_nextItem = "&" & MM_item & "="
  If (InStr(1,MM_removeList,MM_nextItem,1) = 0) Then
    MM_keepForm = MM_keepForm & MM_nextItem & Server.URLencode(Request.Form(MM_item))
  End If
Next

' create the Form + URL string and remove the intial '&' from each of the strings
MM_keepBoth = MM_keepURL & MM_keepForm
If (MM_keepBoth <> "") Then
  MM_keepBoth = Right(MM_keepBoth, Len(MM_keepBoth) - 1)
End If
If (MM_keepURL <> "")  Then
  MM_keepURL  = Right(MM_keepURL, Len(MM_keepURL) - 1)
End If
If (MM_keepForm <> "") Then
  MM_keepForm = Right(MM_keepForm, Len(MM_keepForm) - 1)
End If

' a utility function used for adding additional parameters to these strings
Function MM_joinChar(firstItem)
  If (firstItem <> "") Then
    MM_joinChar = "&"
  Else
    MM_joinChar = ""
  End If
End Function
%>
<%
' *** Move To Record: set the strings for the first, last, next, and previous links

Dim MM_keepMove
Dim MM_moveParam
Dim MM_moveFirst
Dim MM_moveLast
Dim MM_moveNext
Dim MM_movePrev

Dim MM_urlStr
Dim MM_paramList
Dim MM_paramIndex
Dim MM_nextParam

MM_keepMove = MM_keepBoth
MM_moveParam = "index"

' if the page has a repeated region, remove 'offset' from the maintained parameters
If (MM_size > 1) Then
  MM_moveParam = "offset"
  If (MM_keepMove <> "") Then
    MM_paramList = Split(MM_keepMove, "&")
    MM_keepMove = ""
    For MM_paramIndex = 0 To UBound(MM_paramList)
      MM_nextParam = Left(MM_paramList(MM_paramIndex), InStr(MM_paramList(MM_paramIndex),"=") - 1)
      If (StrComp(MM_nextParam,MM_moveParam,1) <> 0) Then
        MM_keepMove = MM_keepMove & "&" & MM_paramList(MM_paramIndex)
      End If
    Next
    If (MM_keepMove <> "") Then
      MM_keepMove = Right(MM_keepMove, Len(MM_keepMove) - 1)
    End If
  End If
End If

' set the strings for the move to links
If (MM_keepMove <> "") Then
  MM_keepMove = Server.HTMLEncode(MM_keepMove) & "&"
End If

MM_urlStr = Request.ServerVariables("URL") & "?" & MM_keepMove & MM_moveParam & "="

MM_moveFirst = MM_urlStr & "0"
MM_moveLast  = MM_urlStr & "-1"
MM_moveNext  = MM_urlStr & CStr(MM_offset + MM_size)
If (MM_offset - MM_size < 0) Then
  MM_movePrev = MM_urlStr & "0"
Else
  MM_movePrev = MM_urlStr & CStr(MM_offset - MM_size)
End If
%>
<!-- #include file="adm_header.asp" -->

<style>
.courses ul {
	display: inline;
	float: left;
	margin: 0px;
	padding: 0px;
}
.courses li {
	list-style-type: none;
	display: inline;
}
.courses li a {
	width: 92px;
	float: left;
	background-image: url(../images/coursesnav.jpg);
	background-repeat: no-repeat;
	text-align: center;
	margin-right: 4px;
	height: 28px;
	padding-top: 4px;
}
.courses a:hover {
	color: #461A7C;
	
}
</style>
<div>
<script language="javascript" type="text/javascript">
<!--
function valQual(){
	if(!document.frmQual.keyword.value){
		alert("Please enter keyword(s) to search for")
		return false;
	}
	else
	{
		return true;
	}
}
//-->
</script>

<div class="file">

<%
if session("courseWork") = "Edited" then
	%><h3 style="color: #FF0000; font-weight:bold;">Course successfully updated. <a href="qual_edit.asp?cid=<%= session("courseWorkID") %>" style="color: #FF0000; text-decoration:underline;">Click here to re-edit course</a>.</h3><%
	session("courseWork") = null
	session("courseWorkID") = null
elseif session("courseWork") = "Added" then
	%><h3 style="color: #FF0000; font-weight:bold;">Course successfully added. <a href="qual_edit.asp?cid=<%= session("courseWorkID") %>" style="color: #FF0000; text-decoration:underline;">Click here to re-edit course</a>.</h3><%
	session("courseWork") = null
	session("courseWorkID") = null
else
	session("courseWork") = null
end if
%>
<a name="coursepoint"></a>


<h2 class="page-title">Products</h2>

<div class="table">
<br />
<a href="qual_add.asp" style=" float:right; color: #C8004C; font-weight:bold; text-decoration:underline;">Create New Product</a>

<table width="100%" border="1">
<%
looper = 0
sectorStr = ""
titleStr = ""
While ((Repeat1__numRows <> 0) AND (NOT cqfRS.EOF))
	

	if looper = 0 then
%>
<tr>
<th scope="col">&nbsp;</th>
<th width="60%" scope="col">Product Name</th>
<th scope="col">&nbsp;</th>

</tr>
<%
		looper = 1
	end if
q_id = (cqfRS.Fields.Item("q_id").Value)
%>
<tr>
<td><a href="#" onClick="return deleteQual('<%= q_id %>');"><img src="images/delete.gif" border="0" /></a></td>
<td><%
qual_title = (cqfRS.Fields.Item("title").Value)
if len(qual_title)>0 then
	response.write qual_title
else
	response.write "&nbsp;"
end if
%></td>


<td nowrap><a href="qual_edit.asp?cid=<%= q_id %>">Edit Product</a></td>
</tr>
<%
	Repeat1__index=Repeat1__index+1
	Repeat1__numRows=Repeat1__numRows-1
	cqfRS.MoveNext()
Wend
%>
</table>

</div>
</div>
</div>
<!-- #include file="adm_footer.asp" -->