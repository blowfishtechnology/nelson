﻿<!-- #include file="adm_header.asp" -->
<%
section_id = request("id")

if section_id = "" then
	response.redirect "adm_menu.asp"
elseif section_id = "new" then
	section_id = "new"
else
	conSQL="SELECT * FROM section WHERE section_id = " & section_id
	set recordset = conn.execute(conSQL)
	do while not recordset.eof
		section_id = recordset("section_id")
		section_name = recordset("section_name")  
    	section_info = recordset("section_info")        
		recordset.movenext
	loop
	recordset.close
end if

if section_id = "new" then
	page_title = "Adding a New Product"
else
	page_title = "Editing a Product"
end if
%>
<div>
<form name="form1" method="post" action="doc_upload_section_page_edit.asp">
<input type="hidden" name="section_id" value="<%= section_id %>">
<h2 class="page-title">Products - <%= page_title %> </h2>
<h6>Product Title:</h6>
<p><input name="section_name" type="text" size="65" value="<%= section_name %>" maxlength="180" /></p>

<h6>Details:</h6>
<p><textarea name="section_info" cols="50" rows="10" id="Textarea1"><%= section_info %> </textarea></p>



<% if section_id = "new" then %>
<p><input name="submit" type="submit" value="Save this section"  class="button_text2"></p>
<% else %>
<p><input name="submit" type="submit" value="Save these changes" class="button_text2"></p>
<% end if %>
</form>
</div>

<!-- #include file="adm_footer.asp" -->