<% page_title = "Photo Gallery" %>
<!-- #include file="adm_header.asp" -->
<%
gal_s = -1
SQL = "SELECT * FROM files ORDER BY section_id, file_id DESC"
set recordset = conn.execute(SQL)
do while not recordset.eof
	gal_s = gal_s + 1
	redim preserve galArr(2,gal_s)
	galArr(0,gal_s) = recordset("file_id")
	galArr(1,gal_s) = recordset("section_id")
	galArr(2,gal_s) = recordset("file_name")
	recordset.movenext
loop
recordset.close

galx_s = -1
SQL="SELECT * FROM section ORDER BY section_name"
set recordset = conn.execute(SQL)
do while not recordset.eof
	galx_s = galx_s + 1
	redim preserve gallArr(1,galx_s)
	gallArr(0,galx_s) = recordset("section_id")
	gallArr(1,galx_s) = recordset("section_name")
	recordset.movenext
loop
recordset.close
%>

<div id="upForm">
<table width="600">
<form name="form1" method="post" action="doc_upload_files.asp" enctype="multipart/form-data">
<tr>
<td width="300" valign="top">
<h3>Uploaded Photos:</h3>
(Click for Options)<br />
<%
for iGal = 0 to galx_s
	docDisplayStr = null
	docDisplayStr = docDisplayStr & "<h3>" & gallArr(1,iGal) & "</h3>" & VbCrLf
	docDisplayStr = docDisplayStr & "<ul>" & VbCrLf
	chkVar = 0
	for ix= 0 to gal_s
		if CInt(gallArr(0,iGal)) = CInt(galArr(1,ix)) then
			chkVar = chkVar + 1
			docDisplayStr = docDisplayStr & "<li style=""padding: 3px;""><a href=""doc_upload_options.asp?img=" & galArr(0,ix) & """>" & galArr(2,ix) & "</a></li>" & VbCrLf
		end if
	next
	docDisplayStr = docDisplayStr & "</ul>" & VbCrLf
	if chkVar > 0 then
		response.write docDisplayStr
	end if
next
%>
</td>
<td width="300" valign="top">
<h3>Photo Gallery Section</h3>
<div><select name="section_id">
<%
for iGal = 0 to galx_s
	%><option value="<%= gallArr(0,iGal) %>"<%= optionStr %>><%= gallArr(1,iGal) %></option><%
	response.write VbCrLf
next
%>
</select></div>
<h3>Select photo(s) for upload:</h3>
<div>
<%
totFields = 6
for i = 1 to totFields
%>
<p><input type="file" name="file<%= i %>" size="40" /></p>
<% next %>
<p><input name="submit" type="submit" value="Upload Now!" onClick="return fireUpload();" /></p>
</div>
<div>&bull;&nbsp;&nbsp;<a href="menu.asp">Return to Main Menu</a></div>
</td>
</tr>
</form>
</table>
</div>
<div id="upWarning">
<h1>Please wait while your photos are uploaded.</h1>
<h1>Depending on filesizes, this may take some time.</h3>
</div>

<!-- #include file="adm_footer.asp" -->