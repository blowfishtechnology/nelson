<% page_title = calendarName %>
<!-- #include file="adm_header.asp" -->
<%

if request.querystring("add") = "do" then
  Set Rs = Server.CreateObject("ADODB.RecordSet")
  rs.open "calendarEvents", strConn, 1,3
  rs.addnew
  dim dateLong
  dateLong = Request.Form("date") & " " & Request.Form("month") & " " & Request.Form("year")
  rs.fields("eventId") = Request.Form("eventId")
  id = request.form("eventId")
  rs.fields("day") = Request.Form("day")
  rs.fields("date") = dateLong
  rs.fields("time") = Request.Form("time")
  rs.fields("avail") = 1
  rs.fields("title") = Request.Form("title")
  rs.fields("slant") = Request.Form("slant")
  if Request.Form("IsFree") = "on" then
  	rs.fields("IsFree") = "true"
  end if
   if Request.Form("AccesstoHE") = "on" then
  	rs.fields("AccesstoHE") = "true"
  end if
  rs.update
  rs.close
  set rs=nothing
response.redirect("editCalendarEvents.asp?id=" & id & "")
end if

id=request.querystring("id")
set ServiceRS = conn.execute("SELECT * FROM calendar WHERE id = " & id & "")
if not ServiceRs.bof and not ServiceRs.eof then
	ServiceRs.movefirst
	calendarName = ServiceRs("name")
end if
%>
<script type='text/javascript' language='Javascript'>
<!--
function valFrm(){
  if(document.frm.day.value=="null"){
    alert("Please select a day!");
	document.frm.day.focus();
	return false;
  }else{
    return valDate();
  }
}
function valDate(){
	if (document.frm.date.value == "null" || document.frm.month.value == "null" || document.frm.year.value == "null"){
		alert("The date is incorrect. Please check a value is selected for date, month & year");
		document.frm.date.focus();
		return false;
	}else{
		return valTime();
	}
}
function valTime(){
  if(!document.frm.time.value){
    alert("Please enter a time for the event!");
	document.frm.time.focus();
	return false;
  }else{
    return true;
  }
}
function deleteItem(id,date){
	if (confirm("Are you sure you want to delete " + date + "?")){
		window.location = "deleteEvent.asp?id="+id;
	}else{
	}
}
//-->
</script>


		  <form name="frm" method="post" action="?add=do" onsubmit="return valFrm();">
		  <input type="hidden" name="eventId" value="<%=id%>">
		   <h4>Add a New Date 	 </h4>
			<table width="100%" cellpadding="0" cellspacing="0" id="no-border-table">
			<tr>
			<td>
		    <p>Day</p>
		      <select name="day" id="day">
		        <option value="null" selected>Select day...</option>
		        <option value="Monday">Monday</option>
		        <option value="Tuesday">Tuesday</option>
		        <option value="Wednesday">Wednesday</option>
		        <option value="Thursday">Thursday</option>
		        <option value="Friday">Friday</option>
		        <option value="Saturday">Saturday</option>
		        <option value="Sunday">Sunday</option>
		        </select>
             </td>
             <td><p> Date </p>
                <select name="date" id="date">
                <option value="null" selected>Date...</option>
                <option value="01">01</option>
                <option value="02">02</option>
                <option value="03">03</option>
                <option value="04">04</option>
                <option value="05">05</option>
                <option value="06">06</option>
                <option value="07">07</option>
                <option value="08">08</option>
                <option value="09">09</option>
                <option value="10">10</option>
                <option value="11">11</option>
                <option value="12">12</option>
                <option value="13">13</option>
                <option value="14">14</option>
                <option value="15">15</option>
                <option value="16">16</option>
                <option value="17">17</option>
                <option value="18">18</option>
                <option value="19">19</option>
                <option value="20">20</option>
                <option value="21">21</option>
                <option value="22">22</option>
                <option value="23">23</option>
                <option value="24">24</option>
                <option value="25">25</option>
                <option value="26">26</option>
                <option value="27">27</option>
                <option value="28">28</option>
                <option value="29">29</option>
                <option value="30">30</option>
                <option value="31">31</option>
                                          </select>
              <select name="month" id="select2">
                <option value="null" selected>Month...</option>
                <option value="Jan">January</option>
                <option value="Feb">February</option>
                <option value="Mar">March</option>
                <option value="Apr">April</option>
                <option value="May">May</option>
                <option value="Jun">June</option>
                <option value="Jul">July</option>
                <option value="Aug">August</option>
                <option value="Sep">September</option>
                <option value="Oct">October</option>
                <option value="Nov">November</option>
                <option value="Dec">December</option>
                                          </select>
              <select name="year" id="select3">
                <option value="null" selected>Year...</option>
                <option value="10">2010</option>
                <option value="11">2011</option>
                <option value="12">2012</option>
                <option value="13">2013</option>
                <option value="14">2014</option>
                <option value="15">2015</option>
                <option value="16">2016</option>
                <option value="17">2017</option>
                <option value="18">2018</option>
                <option value="19">2019</option>
                <option value="20">2020</option>
                                          </select>
             </td></tr>
			
			<tr>
            <td>  <p>Time</p>
              
              <input name="time" type="text" id="time" size="40">
		</td>
             <td><p> Short Title</p>
          
              <input name="title" type="text" id="title" size="40" maxlength="250"></td>
              </tr>
			<tr>
			 <td> <p> Location </p>
			  
              <input name="slant" type="text" id="slant" size="40" maxlength="250">
			</td>
            <td><p>Free Training?</p>
            <input type="checkbox" name="IsFree" id="isFree"/>
             </tr>

				<tr>
				<td colspan="2">  <p>Show on 'Access to HE' page?</p>
            <input type="checkbox" name="AccesstoHE" id="AccesstoHE"/>
          </td>
          </tr>

				<tr>
				<td colspan="2">
              <input type="submit" name="Submit" value="Save & Add Event" class="button_text2">
			</td>
       		    </tr>
		  </form>
		</table>
		<div class="surroundgreydark">
		  <h5>Current Dates </h5>
		<p>Dates below have previously been added to the system. Delete as necessary below </p>
		  <%
		  set rs = conn.execute("SELECT * FROM calendarEvents WHERE eventId = " & id & " ORDER BY date ASC")
		  if rs.eof then
		  response.write "There are currently no events listed"
		  else
		  %>
		  <table width="100%"  border="0" cellspacing="0" cellpadding="0">
            <tr>
              <% if id = "7" then %>
              	<td>>Title</td>
              <% end if %>
              <th>Location</th>
              <th>Day</th>
              <th>Date</th>
              <th>Time</th>
              <th>Delete</th>
              <th>Click for booked</th>
            </tr>
			<%
			do while not rs.eof
				event_id = rs("id")
			%>
            <tr bgcolor="#C8004C">
              <% if id = "7" then %>
              	<td><%Response.write rs("title")%></td>
              <% end if %>
              <td><%Response.write rs("slant")%></td>
              <td><%Response.write rs("day")%></td>
              <td><%Response.write rs("date")%></td>
              <td><%Response.write rs("time")%></td>
              <td><a href="javascript:deleteItem('<%Response.Write rs("id")%>','<%Response.Write rs("date")%>')" class="delete">Delete</a></td>
              <td><a href="eventAvail.asp?id=<%= event_id %>"><%
				if rs("avail") = True then
					response.write "Places Available"
				else
					response.write "Course Full"
				end if
              %></a></td>
            </tr>
			<%
			rs.movenext
			loop
			%>
          </table>
		  <%
		  end if
		  set rs=nothing
		  call closeConn()
		  %>
		</div>
<!-- #include file="adm_footer.asp" -->