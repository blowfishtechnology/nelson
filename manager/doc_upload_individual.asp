﻿<% page_title = "Uploading Carousel Images" %>
<!-- #include file="adm_header.asp" -->
<%
gal_s = -1
section_id = request("id")

SQL = "SELECT * FROM [files] WHERE section_id = " & section_id
set recordset = conn.execute(SQL)
do while not recordset.eof
	gal_s = gal_s + 1
	redim preserve galArr(2,gal_s)
	galArr(0,gal_s) = recordset("file_id")
	galArr(1,gal_s) = recordset("section_id")
	galArr(2,gal_s) = recordset("file_name")
	recordset.movenext
loop
recordset.close

galx_s = -1
SQL="SELECT * FROM section WHERE section_id = " & section_id
set recordset = conn.execute(SQL)
do while not recordset.eof
	galx_s = galx_s + 1
	redim preserve gallArr(1,galx_s)
	gallArr(0,galx_s) = recordset("section_id")
	gallArr(1,galx_s) = recordset("section_name")
	recordset.movenext
loop
recordset.close
%>

<div id="upForm">
<table width="600">
<form name="form1" method="post" action="doc_upload_files.asp?id=<%= section_id %>" enctype="multipart/form-data">
<tr>
<td width="300" valign="top">
<h3>Uploaded Images:</h3>
(Click photo for Options)<br />
<%
for iGal = 0 to galx_s
	docDisplayStr = null
	'docDisplayStr = docDisplayStr & "<h3>" & gallArr(1,iGal) & "</h3>" & VbCrLf
	docDisplayStr = docDisplayStr & "<ul>" & VbCrLf
	chkVar = 0
	for ix= 0 to gal_s
		if CInt(gallArr(0,iGal)) = CInt(galArr(1,ix)) then
			chkVar = chkVar + 1
			docDisplayStr = docDisplayStr & "<img src='../../files/" & galArr(2,ix) &"' width='100px'><li style=""padding: 3px;""><a href=""doc_upload_options.asp?img=" & galArr(0,ix) & """>" & galArr(2,ix) & "</a>&nbsp; <a href=""doc_upload_options.asp?img=" & galArr(0,ix) & """>Edit</a></li>" & VbCrLf
		end if
	next
	docDisplayStr = docDisplayStr & "</ul>" & VbCrLf
	if chkVar > 0 then
		response.write docDisplayStr
	end if
next
%>
</td>
<td width="300" valign="top" style="position:relative;vertical-align:top;">

<%
for iGal = 0 to galx_s
	%>        
<input name="section_id" type="hidden" value="<%= gallArr(0,iGal) %>" />
    <%	
next
%>

<h3>Select photo(s) for upload:</h3>

<%
totFields = 6
for i = 1 to totFields
%>
<p><input type="file" name="file<%= i %>" size="40" /></p>
<% next %>
<p><input name="submit" type="submit" value="Upload Now!" onClick="return fireUpload();" /></p>

<div>&bull;&nbsp;&nbsp;<a href="menu.asp">Return to Main Menu</a></div>
</td>
</tr>
</form>
</table>
</div>
<div id="upWarning">
<h1><img src="files/waiting.gif">Please wait while your photos are uploaded.</h1>
<h1>Depending on photo size, this may take some time. </h3>
</div>

<!-- #include file="adm_footer.asp" -->