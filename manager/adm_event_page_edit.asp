<% page_title = "Editing an Event" %>
<!-- #include file="adm_header.asp" -->
<%
PathFolder = Server.MapPath("../images")
Set Upload = Server.CreateObject("Persits.Upload.1")
Set Jpeg = Server.CreateObject("Persits.Jpeg")
Upload.OverwriteFiles = False
Count = Upload.SaveToMemory

news_id = Upload.Form("news_id")
if requestSafe(news_id) = 1 then response.redirect "adm_event_list.asp" end if

news_title = Upload.form("news_title") ' = this is a headline
news_day = Upload.form("news_day") ' = 23
news_month = Upload.form("news_month") ' = Jan
news_year = Upload.form("news_year") ' = 2007
news_summary = Upload.form("news_summary") ' = this is a summary
news_article = Upload.form("news_article") ' = asfgadpl adfpgk a dfog 'skdfg sd[ gsdf g
news_type_id = Upload.form("news_type_id")
submitStr = Upload.form("submit") ' = Save these changes

if submitStr = "Save these changes" then

	SQL="UPDATE events SET " &_
		"news_title = '" & replace(news_title,"'","&#146;") & "', " &_
		"news_date = '" & day(now()) & "-" & monthname(month(now())) & "-" & year(now()) & "', " &_
		"news_summary = '" & replace(news_summary,"'","&#146;") & "', " &_
		"news_article = '" & replace(news_article,"'","&#146;") & "', " &_
		"news_type_id = '" & replace(news_type_id,"'","&#146;") & "' " &_
		"WHERE news_id = " & news_id
	set recordset = conn.execute(SQL)

end if

if Count > 0 then
	for each file in Upload.Files
' ************************************************************************************************ '
		Path = PathFolder & "\" & file.ExtractFileName
		file.SaveAs Path
		thumbnail = file.ExtractFileName
' ************************************************************************************************ '
' ********** THUMBNAIL *************************************************************************** '
' ************************************************************************************************ '
		Jpeg.Open PathFolder & "\" & thumbnail
 		thumbWidth = 318
		Jpeg.quality = 95
 		ow = jpeg.OriginalWidth
 		oh = jpeg.OriginalHeight

		if ow > thumbWidth then
			jpeg.Width = thumbWidth
			jpeg.Height = oh * thumbWidth / ow
		end if

		Jpeg.Save PathFolder & "\" & thumbnail
' ************************************************************************************************ '
' ********** MAIN IMAGE ************************************************************************** '
' ************************************************************************************************ '
'		mainImage = file.ExtractFileName
'
'		maxDimensionWidth = 450
'
'		Jpeg.Open PathFolder & "\" & mainImage
		Jpeg.quality = 95
'		ow = jpeg.OriginalWidth
'		oh = jpeg.OriginalHeight
'		if ow > maxDimensionWidth then
'			jpeg.Width = maxDimensionWidth
'			jpeg.Height = oh * maxDimensionWidth / ow
'		End If
'		Jpeg.Save PathFolder & "\" & mainImage
' ************************************************************************************************ '

	SQL="UPDATE events SET " &_
		"news_image = '" & replace(thumbnail,"'","") & "' " &_
		"WHERE news_id = " & news_id
	conn.execute(SQL)

	next
end if

conSQL="SELECT * FROM events WHERE news_id = " & news_id
set recordset = conn.execute(conSQL)
do while not recordset.eof
	news_id = recordset("news_id")
	news_title = recordset("news_title")
	news_date = recordset("news_date")
	news_summary = recordset("news_summary")
	news_article = recordset("news_article")
	news_image = recordset("news_image")
	news_type_id = recordset("news_type_id")
	recordset.movenext
loop
recordset.close
%>
<h1>Event Successfully Updated</h1>
<br />

<div>
<strong>Title:</strong>
<p><%= news_title %></p>
<strong>    Venue:</strong>
<p><%= news_summary %></p>

<strong>Dates:</strong>
<p><%= news_article %></p>
</div>

<hr />

<p><strong><a href="adm_event_list.asp">Return to Events Menu</a></strong></p>

<!-- #include file="adm_footer.asp" -->