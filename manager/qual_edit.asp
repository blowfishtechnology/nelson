<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<!-- #include file="include_authenticate.asp" -->
<!-- #include file="ocnser.asp" -->
<!-- #include file="date_function.asp" -->
<%
actionStr = Request("action")

'Dim Connection
'Set Connection = Server.CreateObject("ADODB.Connection")
'Connection.Open MM_ocnni_STRING

If actionStr = "GO" Then

	Set Upload = Server.CreateObject("Persits.Upload.1")
	Upload.OverwriteFiles = False
	Count = Upload.SaveToMemory


	PathFolder = Server.MapPath("../quals")

	filename = ""

	For Each File in Upload.Files
		Path = PathFolder & "\" & File.ExtractFileName
		File.SaveAs Path
		filename = File.ExtractFileName
	next

	course_id = upload.form("q_id")
	If requestSafe(course_id) = 1 Then
		response.redirect "qual_list.asp"
	End If

	
	title = upload.form("title") ' = sdf
	description = upload.form("description") ' = dfgh




	SQL="UPDATE qualification SET " &_
		"title = '" & sqlSafe(title) & "', " &_	
		"description = '" & sqlSafe(description) & "' " &_
		"WHERE q_id = " & course_id
	Set RecordSet = Connection.Execute(SQL)

	if len(filename) > 0 then
		SQL="UPDATE qualification SET " &_
			"filename = '" & sqlSafe(filename) & "' " &_
			"WHERE q_id = " & course_id
		Set RecordSet = Connection.Execute(SQL)
	end if


	response.redirect "qual_list.asp"
End If


course_id = Request("cid")


SQL="SELECT * FROM qualification WHERE q_id = " & course_id
Set RecordSet = Connection.Execute(SQL)
do while not RecordSet.EOF
	q_id = RecordSet("q_id")
	title = RecordSet("title")	
	description = RecordSet("description")
	filename = RecordSet("filename")
	RecordSet.MoveNext
loop
RecordSet.Close



%>
<!-- #include file="adm_header.asp" -->

<div style="padding: 5px; width: 500px; text-align:center;" class="style3">Adding/Editing a New Product</div>
<br />

<table>
<form method="post" action="?action=GO&cid=<%= course_id %>" name="form1" ENCTYPE="multipart/form-data">
<input type="hidden" name="q_id" value="<%= q_id %>" />

<tr valign="baseline">
<td nowrap align="right">Product Name:</td>
<td><input type="text" name="title" value="<%= title %>" size="50" /></td>
</tr>
<tr valign="baseline">
<td nowrap align="right" valign="top">Description:</td>
<td><%
Dim oFCKeditor
Set oFCKeditor = New FCKeditor
oFCKeditor.BasePath = ""
oFCKeditor.Value = description
oFCKeditor.height = 250
oFCKeditor.width = 400
oFCKeditor.ToolbarSet = "Basic"
oFCKeditor.Create "description"
%></td>
</tr>
<% if filename <> "" then %>
<tr valign="baseline">
<td nowrap align="right">Uploaded File:</td>
<td><a href="../quals/<%= filename %>" target="_blank"><img src="../images/pdf.gif" border="0" /></a></td>
</tr>
<% end if %>
<tr valign="baseline">
<td nowrap align="right">Upload a file:</td>
<td><input type="file" name="filename" size="50" /> <%= filename %></td>
</tr>
<tr valign="baseline">
<td nowrap align="right">&nbsp;</td>
<td><input type="submit" value="Update This Product"></td>
</tr>
</table>
</form>

<!-- #include file="adm_footer.asp" -->