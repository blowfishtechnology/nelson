<% page_title = "Adding an Event" %>
<!-- #include file="adm_header.asp" -->
<%



PathFolder = Server.MapPath("../images")
Set Upload = Server.CreateObject("Persits.Upload.1")
Set Jpeg = Server.CreateObject("Persits.Jpeg")
Upload.OverwriteFiles = False
Count = Upload.SaveToMemory

news_title = Upload.form("news_title") ' = this is a headline
news_day = Upload.form("news_day") ' = 23
news_month = Upload.form("news_month") ' = Jan
news_year = Upload.form("news_year") ' = 2007
news_summary = Upload.form("news_summary")
news_article = Upload.form("news_article")
news_type_id = Upload.form("news_type_id")
submitStr = Upload.form("submit") ' = Save this article


	If Upload.Form("AccesstoHE") = "on" Then 
'Response.Write "checked" 
 AccesstoHEbool = 1
Else 
'Response.Write "unchecked"
   AccesstoHEbool = 0
End If


if submitStr = "Save this event" then


if Count > 0 then
	for each file in Upload.Files
' ************************************************************************************************ '
		Path = PathFolder & "\" & file.ExtractFileName
		file.SaveAs Path
		thumbnail = file.ExtractFileName
' ************************************************************************************************ '
' ********** THUMBNAIL *************************************************************************** '
' ************************************************************************************************ '
		Jpeg.Open PathFolder & "\" & thumbnail
 		thumbWidth = 318
		Jpeg.quality = 95
 		ow = jpeg.OriginalWidth
 		oh = jpeg.OriginalHeight
		
		if ow > thumbWidth then
			jpeg.Width = thumbWidth
			jpeg.Height = oh * thumbWidth / ow
		end if

		Jpeg.Save PathFolder & "\" & thumbnail
' ************************************************************************************************ '
' ********** MAIN IMAGE ************************************************************************** '
' ************************************************************************************************ '
'		mainImage = file.ExtractFileName
'
'		maxDimensionWidth = 450
'
'		Jpeg.Open PathFolder & "\" & mainImage
		Jpeg.quality = 95
'		ow = jpeg.OriginalWidth
'		oh = jpeg.OriginalHeight
'		if ow > maxDimensionWidth then
'			jpeg.Width = maxDimensionWidth
'			jpeg.Height = oh * maxDimensionWidth / ow
'		End If
'		Jpeg.Save PathFolder & "\" & mainImage
' ************************************************************************************************ '

	next
end if


	SQL="INSERT INTO events(news_title, news_date, news_summary, news_article, news_image, AccesstoHE, news_type_id) " &_
		"VALUES (" &_
		"'" & replace(news_title,"'","&#146;") & "', " &_
		"'" & day(now()) & "-" & monthname(month(now())) & "-" & year(now()) & "', " &_
		"'" & replace(news_summary,"'","&#146;") & "', " &_
		"'" & replace(news_article,"'","&#146;") & "', " &_
		"'" & replace(thumbnail,"'","") & "', " &_
		"" & AccesstoHEbool & ", " &_
        "" & replace(news_type_id,"'","&#146;") & ")"

	set recordset = conn.execute(SQL)

end if
%>

<h1>Event Successfully added.</h1>
<br />

<div>
<strong>Title:</strong>
<p><%= news_title %></p>
<strong>Venue:</strong>
<p><%= news_summary %></p>

<strong>Dates:</strong>
<p><%= news_article %></p>
</div>

<hr />

<p><strong><a href="adm_event_list.asp">Return to Events</a></strong></p>

<!-- #include file="adm_footer.asp" -->