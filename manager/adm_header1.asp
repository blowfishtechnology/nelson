﻿<% Response.CharSet = "UTF-8" %>
<%
response.buffer=true
Server.ScriptTimeout = 9999
Dim conn,rs,servicers, strConn
%>
<!-- #include file="include_authenticate.asp" -->
<!-- #include file="include_dbconnect.asp" -->
<!-- #include file="../conex/date_function.asp" -->
<%
call openConn()

function sqlSafe(passthru)
	if len(passthru)>0 then
		sqlSafe = replace(passthru,"'","&#146;")
	else
		sqlSafe = ""
	end if
end function

function requestSafe(passthru)
	stepthru = 0
	if IsEmpty(passthru) then
		stepthru = 1
	end if
	if IsNull(passthru) then
		stepthru = 1
	end if
	if passthru = "" then
		stepthru = 1
	end if
	if stepthru = 0 then
		if IsNumeric(CStr(passthru)) then
			stepthru = 0
		else
			stepthru = 1
		end if
	end if
	if len(passthru) > 8 then
		stepthru = 1
	end if
	requestSafe = stepthru
end function
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />


<title>Content Management System</title>
<link href="css/manager.css" rel="stylesheet" type="text/css" />
<link href="css/adminstyle.css" rel="stylesheet" type="text/css" />
<script language="JavaScript" type="text/JavaScript">
<!--
function deleteCWUser(querystring)
{
	input_box=confirm("Click OK or delete this Credit Works user permanently\nor Cancel to keep it.");
	if (input_box==true)
	{
		// Output when OK is clicked
		window.location = "sec_securityDel.asp?user_id="+querystring
	}
}

function deleteTestimonial(querystring)
{
	input_box=confirm("Click OK or delete this Testimonial permanently\nor Cancel to keep it.");
	if (input_box==true)
	{
		// Output when OK is clicked
		window.location = "testimonials_delete.asp?id="+querystring
	}
}
function deleteFAQ(querystring)
{
	input_box=confirm("Click OK or delete this FAQ permanently\nor Cancel to keep it.");
	if (input_box==true)
	{
		// Output when OK is clicked
		window.location = "faq_delete.asp?id="+querystring
	}
}
function deleteUpload(querystring)
{
	input_box=confirm("Click OK or delete this file permanently\nor Cancel to keep it.");
	if (input_box==true)
	{
		// Output when OK is clicked
		window.location = "doc_upload_delete.asp?img="+querystring
	}
}
function deleteImage(querystring)
{
	input_box=confirm("Click OK or delete this file permanently\nor Cancel to keep it.");
	if (input_box==true)
	{
		// Output when OK is clicked
		window.location = "adm_upload_delete.asp?img="+querystring
	}
}
function deleteNews(querystring)
{
	input_box=confirm("Click OK or delete this news article permanently\nor Cancel to keep it.");
	if (input_box==true)
	{
		// Output when OK is clicked
		window.location = "adm_news_delete.asp?id="+querystring
	}
}

function deleteEvent(querystring) {
    input_box = confirm("Click OK or delete this event permanently\n or Cancel to keep it.");
    if (input_box == true) {
        // Output when OK is clicked
        window.location = "adm_event_delete.asp?id=" + querystring
    }
}



function deleteProject(querystring) {
    input_box = confirm("Click OK or delete this project permanently\n or Cancel to keep it.");
    if (input_box == true) {
        // Output when OK is clicked
        window.location = "adm_project_delete.asp?id=" + querystring
    }
}
function deleteSection(querystring)
{
	input_box=confirm("Click OK or delete this document section permanently\nor Cancel to keep it.");
	if (input_box==true)
	{
		// Output when OK is clicked
		window.location = "doc_upload_section_delete.asp?g="+querystring
	}
}
function deleteCategory(querystring)
{
	input_box=confirm("Click OK or delete this document section permanently\nor Cancel to keep it.");
	if (input_box==true)
	{
		// Output when OK is clicked
		window.location = "adm_section_delete.asp?g="+querystring
	}
}
function deleteSize(querystring)
{
	input_box=confirm("Click OK or delete this size permanently\nor Cancel to keep it.");
	if (input_box==true)
	{
		// Output when OK is clicked
		window.location = "adm_size_deletecode.asp?id="+querystring
	}
}
function deletePType(querystring)
{
	input_box=confirm("Click OK or delete this product type permanently\nor Cancel to keep it.");
	if (input_box==true)
	{
		// Output when OK is clicked
		window.location = "adm_ptype_deletecode.asp?id="+querystring
	}
}
function deleteGroup(querystring)
{
	input_box=confirm("Click OK or delete this group permanently\nor Cancel to keep it.");
	if (input_box==true)
	{
		// Output when OK is clicked
		window.location = "cms_group_deletecode.asp?id="+querystring
	}
}
function fireUpload()
{
	document.getElementById("upForm").style.display = "none";
	document.getElementById("upWarning").style.display = "block";
	return true;
}

function deleteUnit(querystring)
{
	input_box=confirm("Click OK or delete this unit permanently\nor Cancel to keep it.");
	if (input_box==true)
	{
		// Output when OK is clicked
		window.location = "course_del.asp?id="+querystring
	}
}
function deleteNewUnit(querystring)
{
	input_box=confirm("Click OK or delete this unit permanently\nor Cancel to keep it.");
	if (input_box==true)
	{
		// Output when OK is clicked
		window.location = "course_new_del.asp?id="+querystring
	}
}
function deleteQual(querystring)
{
	input_box=confirm("Click OK or delete this qualification permanently\nor Cancel to keep it.");
	if (input_box==true)
	{
		// Output when OK is clicked
		window.location = "qual_del.asp?id="+querystring
	}
}

-->
</script>
</head>

<body>
<!-- #include file="fckeditor.asp" -->

<div id="wrapper">
	<div id="header">

<div id="top-nav">	<ul> 
<li><a href="menu.asp">Admin Home</a> </li>
<li> <a href="logout.asp">Logout</a> </li> 
<li> <a href="http://nr.ballymena.gov.uk/" target="_blank">Main Site</a></li>
</ul></div>


</div>
<!-- end #header -->
<div id="content-surround" >
<% ' ************************************************************************************************************************ ' %>
<!-- #include file="_manager_nav.asp" -->
<% ' ************************************************************************************************************************ ' %>
<div id="primary-content">
<h2 class="page-title"><%= page_title %></h2>
<!-- start #mainContent -->