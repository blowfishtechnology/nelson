
<!-- #include file="adm_header.asp" -->
<%
id = request.querystring("id")

if requestSafe(id) = 1 then
	id = 0
end if

call openConn()

contentSQL = "SELECT * FROM content WHERE id = " & id
set RecordSet = conn.Execute(contentSQL)
do until RecordSet.eof
	parent_id = RecordSet("parent_id")
	name1 = RecordSet("name")
	content = RecordSet("content")
	RecordSet.MoveNext
Loop
RecordSet.Close
%>

<% page_title = name1 %>

<h2 class="page-title"><%= name1 %></h2>

<script language="JavaScript1.1" src="adm_content.js"></script>
<form name="frmcontent" method="post" action="adm_content_editcode.asp">
<input type="hidden" name="id" value="<%= id %>" />
<input type="hidden" name="parent_id" value="<%= parent_id %>" />

<table width="100%" id="no-border-table">

<tr>
<td><h6>Title</h6>
<input name="name" value="<%= name1 %>" >
</td>
</tr>

<tr>
<td><h6>Page Content</h6>
<%
Dim oFCKeditor
Set oFCKeditor = New FCKeditor
oFCKeditor.BasePath = ""
oFCKeditor.Value = content
oFCKeditor.height = 500
oFCKeditor.width = 630
oFCKeditor.ToolbarSet = "OCNNIToolbar"
oFCKeditor.Create "content" 
 %>

</td>
</tr>
<tr>
<td colspan="2"><input type="submit" class="button_text2" name="Submit" value="Save Now" /></td>
</tr>
</table>
</form>

<!-- #include file="adm_footer.asp" -->