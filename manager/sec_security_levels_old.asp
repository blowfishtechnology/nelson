<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<!-- #include file="include_authenticate.asp" -->
<!--#include file="connect.asp" -->
<%
user_id = Request.QueryString("user_id")
if requestSafe(user_id) = 1 then
	user_id = 0
end if

looper = -1
SQLtext="SELECT section_id FROM siteuser_sitelevel WHERE user_id = " & user_id
set securityRS = Connection.Execute(SQLtext)
do until securityRS.eof
	looper = looper + 1
	redim preserve section(looper)
	section(looper) = securityRS("section_id")
	securityRS.Movenext
Loop
securityRS.Close

spinner = -1
SQLtext="SELECT level_id, [level] FROM [sitelevel] ORDER BY [level_id]"
set levelsRS = Connection.Execute(SQLtext)
do until levelsRS.eof
	spinner = spinner + 1
	redim preserve levels(1,spinner)
	levels(0,spinner) = levelsRS("level_id")
	levels(1,spinner) = levelsRS("level")
	levelsRS.Movenext
Loop
levelsRS.Close

SQLtext="SELECT * FROM siteuser WHERE user_id = " & user_id
set userRS = Connection.Execute(SQLtext)
do until userRS.eof
	username = userRS("username")
	uploadFlag = userRS("uploadFlag")
	commentFlag = userRS("commentFlag")
	userRS.Movenext
Loop
userRS.Close

Connection.Close
%>
<%
page_title = "Editing Permissions for " & username
%>
<!--#include file="adm_header.asp" -->

<br />

<form method="post" action="sec_security_levels_code.asp" name="form1">
<input type="hidden" name="user_id" value="<%= user_id %>">
<table align="center" cellpadding="2" cellspacing="2" class="boxer">
<tr>
<td valign="top">

<table cellpadding="2" cellspacing="2">
<tr valign="baseline">
<td align="center"><strong>Sections</strong></td>
</tr>
<% for i = 0 to spinner %>
<tr valign="baseline">
<td><input type="checkbox" name="level_id" value="<%= levels(0,i) %>"<%
	for foo = 0 to looper
		if section(foo) = levels(0,i) then
			response.write " checked"
		end if
	next
%>><%= levels(1,i) %></td>
</tr>
<% next %>
<tr valign="baseline">
<td> <input type="submit" name="submit" value="Update Record"> </td>
</tr>
</table>

</td>
</tr>
</table>
</form>
<p>&nbsp;</p>
<!-- #include file="adm_footer.asp" -->