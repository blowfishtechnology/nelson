<!--#include file="include_authenticate.asp" -->
<!--#include file="../conex/ocnser.asp" -->
<%
news_id = request("id")
if requestSafe(news_id) = 1 then
	response.redirect "adm_news_list.asp"
else
	SQL="SELECT archiveFlag FROM news WHERE news_id = " & news_id
	set recordset = Connection.Execute(SQL)
	do while not recordset.eof
		archiveFlag = recordset("archiveFlag")
		recordset.movenext
	loop
	recordset.close

	if (archiveFlag) then
		SQL="UPDATE news SET archiveFlag = 0 WHERE news_id = " & news_id
		reDirStr = "adm_news_archive_list.asp"
	else
		SQL="UPDATE news SET archiveFlag = 1 WHERE news_id = " & news_id
		reDirStr = "adm_news_list.asp"
	end if
	Connection.Execute(SQL)

end if

response.redirect reDirStr
%>