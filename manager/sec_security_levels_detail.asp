<!--#include file="adm_header.asp" -->
<!-- #include file="include_authenticate.asp" -->
<!--#include file="connect.asp" -->
<%
user_id = request.form("user_id") ' = 1
uploadFlag = request.form("uploadFlag")
commentFlag = request.form("commentFlag")
level_id = request.form("level_id") ' = 1, 2, 8, 9, 10

session("seccpass") = "0"

MM_ocnni_STRING = "Driver={SQL Server}; Server=" & SQL_Server & "; Database=" & SQL_Database & "; UID=" & SQL_UID & "; PWD=" & SQL_PWD

if NOT (IsObject(Connection)) then

	Dim Connection
	Set Connection = Server.CreateObject("ADODB.Connection")
	Connection.Open MM_ocnni_STRING

end if

user_id = request("user_id")
if requestSafe(user_id) = 1 then
	user_id = 0
end if
level_id = request("id")
if requestSafe(level_id) = 1 then
	level_id = 0
end if

SQLtext="SELECT username FROM siteuser WHERE user_id = " & user_id
set userRS = Connection.Execute(SQLtext)
do until userRS.eof
	username = userRS("username")
	userRS.Movenext
Loop
userRS.Close

hasChildren = false
SQL="SELECT hasChildren FROM [sitelevel] WHERE [level_id] = " & level_id
set RecordSet = Connection.Execute(SQL)
do until RecordSet.eof
	hasChildren = RecordSet("hasChildren")
	RecordSet.Movenext
Loop
RecordSet.Close

if (hasChildren) then

	perm_i = -1
	SQL="SELECT content_id FROM siteuser_sitelevel WHERE user_id = " & user_id & " AND section_id = " & level_id
	set permRS = Connection.Execute(SQL)
	do until permRS.eof
		perm_i = perm_i + 1
		redim preserve permArr(perm_i)
		permArr(perm_i) = permRS("content_id")
		permRS.Movenext
	Loop
	permRS.Close
else

	reDirStr = "sec_security_levels.asp?user_id=" & user_id
	response.redirect reDirStr

end if

PageNameStr = "Editing Permissions for " & username
%>

<style type="text/css">
<!--
#levelContainer {

	font-size: 11px;
}
#levelConstraint {
	width: 620px;
	min-height: 700px;
	background-color: #FFFFFF;
}
.levelfloat {
	float: left;
	width: 190px;
	min-height: 25px;
	display:block;
border:1px solid #eee;
	margin: 3px 3px 3px 3px;
	padding:2px;
}
.levelfloat input {
	vertical-align: baseline;
	height: 9px;
	width: 9px;
}
.levelSubmit {
	clear: both;
	margin: 10px 10px 10px 10px;
	text-align: right;
}
-->
</style>
<SCRIPT LANGUAGE="JavaScript">
<!--
function checkAll(field)
{
for (i = 0; i < field.length; i++)
	field[i].checked = true ;
}

function uncheckAll(field)
{
for (i = 0; i < field.length; i++)
	field[i].checked = false ;
}
//-->
</script>

<h2 class="page-title">Content Areas</h2><%
SELECT CASE session("secsectionedit")
	CASE 1
		%><br><br><p><font color="#ff0000" >Permissions updated!</font></p>&nbsp;&nbsp;
		<a href="sec_security_levels.asp?user_id=<%= user_id %>" class="button_text2">back to permissions menu</a><%
	CASE 2
		%><br><br><p><font color="#ff0000">Permissions NOT updated!</font></p>&nbsp;&nbsp;
		<a href="sec_security_levels.asp?user_id=<%= user_id %>" class="button_text2">back to permissions menu</a><%
END SELECT
session("secsectionedit") = null
%>
<div id="levelContainer">
<form method="post" action="sec_security_levels_detail_code.asp" name="form1" id="form1">
<input type="hidden" name="user_id" value="<%= user_id %>" />
<input type="hidden" name="level_id" value="<%= level_id %>" />

<div class="levelSubmit">
<input type=button name="CheckAll"   value="Check All" onClick="checkAll(document.form1.content_id)">
<input type=button name="UnCheckAll" value="Uncheck All" onClick="uncheckAll(document.form1.content_id)">
<input type="submit" name="submit" value="Update Security" />
</div>
<div class="surroundgreydark">
<%
CreateChildren(0)
sub CreateChildren(Parent_ID)	
	SQL = "Select * from [content] where [edit_item] = 1 and parent_id = " & Parent_ID & " order by shuffle_order asc"
	set ChildRS = Connection.Execute(SQL)
	do until ChildRS.eof
		
		chkStr = null
		if perm_i > -1 then

			for perm_s = 0 to perm_i
				if CINT(ChildRS("id")) = CINT(permArr(perm_s)) then
					chkStr = " checked"
				end if
			next
		end if
		%>
        
        <% if Parent_ID = 0 then %>
		<div class="levelfloat" style="background-color:#1B6D9F; color:white;">
        <% else %>
        <div class="levelfloat">
        <% end if %>
		<input type="checkbox" name="content_id" value="<%=ChildRS("id")%>"<%= chkStr %> />
		&nbsp;
        <%= ChildRS("name") %>
		</div>
    
        <%
		CreateChildren(ChildRS("id"))
		ChildRs.MoveNext
	loop
	ChildRS.close

end sub
%>

<div class="levelSubmit"><input type="submit" name="submit" value="Update Security" /></div>

</div>

</form>
</div>
<p align="center"><a href="sec_security_levels.asp?user_id=<%= user_id %>">back to permissions menu</a></p>
<!-- #include file="adm_footer.asp" -->