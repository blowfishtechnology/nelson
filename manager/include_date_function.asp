<%
'## original code by Chris Burge (chrisb@webkaos.co.uk)
'## code is free to use by all

'# refer to documentation for options codes for the StringStructure
Function kaos_date(ByVal StringStructure,ByVal Timestamp)
'-- validate arguments --
If Not IsDate(Timestamp) Then 'invalid date
 kaos_date = Timestamp
 Exit Function
End If
If InStr(1,StringStructure,"%",0) = 0 Then 'invalid structure
 kaos_date = Timestamp
 Exit Function
End If
Dim zTmpDate1,zTmp1,zTmp2
zTmpDate1 = StringStructure
'# day of month / suffixes #
zTmp1 = Day(Timestamp) 'day of month number
Select Case zTmp1 'day suffix
Case 1,21,31
 zTmp2 = "st"
Case 2,22
 zTmp2 = "nd"
Case 3,23
 zTmp2 = "rd"
Case Else
 zTmp2 = "th"
End Select
zTmpDate1 = Replace(zTmpDate1,"%d",zTmp1,1,-1,0)
zTmpDate1 = Replace(zTmpDate1,"%D",Right(("0" & zTmp1),2),1,-1,0)
zTmpDate1 = Replace(zTmpDate1,"%s",zTmp2,1,-1,0)
zTmpDate1 = Replace(zTmpDate1,"%S",UCase(zTmp2),1,-1,0)
'# weekday name / number #
zTmp1 = Weekday(Timestamp)
zTmp2 = WeekdayName(zTmp1)
zTmpDate1 = Replace(zTmpDate1,"%e",Left(zTmp2,3),1,-1,0)
zTmpDate1 = Replace(zTmpDate1,"%E",zTmp2,1,-1,0)
zTmpDate1 = Replace(zTmpDate1,"%w",zTmp1,1,-1,0)
'# month name / number #
zTmp1 = Month(Timestamp)
zTmp2 = MonthName(zTmp1)
zTmpDate1 = Replace(zTmpDate1,"%m",zTmp1,1,-1,0)
zTmpDate1 = Replace(zTmpDate1,"%M",Right(("0" & zTmp1),2),1,-1,0)
zTmpDate1 = Replace(zTmpDate1,"%n",Left(zTmp2,3),1,-1,0)
zTmpDate1 = Replace(zTmpDate1,"%N",zTmp2,1,-1,0)
'# year / day of year #
zTmp1 = Year(Timestamp)
zTmpDate1 = Replace(zTmpDate1,"%y",Right(CStr(zTmp1),2),1,-1,0)
zTmpDate1 = Replace(zTmpDate1,"%Y",zTmp1,1,-1,0)
zTmpDate1 = Replace(zTmpDate1,"%z",DatePart("y",Timestamp),1,-1,0)

'# times #
'am/pm indicator
zTmp1 = Hour(Timestamp)
If zTmp1 < 12 Then
 zTmp2 = "am"
Else
 zTmp2 = "pm"
End If
zTmpDate1 = Replace(zTmpDate1,"%a",zTmp2,1,-1,0)
zTmpDate1 = Replace(zTmpDate1,"%A",UCase(zTmp2),1,-1,0)
'24hr hour number
zTmpDate1 = Replace(zTmpDate1,"%h",zTmp1,1,-1,0)
zTmpDate1 = Replace(zTmpDate1,"%H",Right(("0" & zTmp1),2),1,-1,0)
'12hr hour number
If zTmp1 > 12 Then
 zTmp1 = zTmp1 - 12
End If
'hour 0 fix, courtesy of Bob Gregorius. thanks Bob :)
If zTmp1 = 0 Then
 zTmp1 = 12
End If
zTmpDate1 = Replace(zTmpDate1,"%g",zTmp1,1,-1,0)
zTmpDate1 = Replace(zTmpDate1,"%G",Right(("0" & zTmp1),2),1,-1,0)
'minutes and seconds
zTmpDate1 = Replace(zTmpDate1,"%t",Right(("0" & Minute(Timestamp)),2),1,-1,0)
zTmpDate1 = Replace(zTmpDate1,"%T",Right(("0" & Second(Timestamp)),2),1,-1,0)

'# return the now parsed date string to the function #
kaos_date = zTmpDate1
End Function
%>