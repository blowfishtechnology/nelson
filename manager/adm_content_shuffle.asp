<!-- #include file="include_dbconnect.asp" --><!-- #Include file="../conex/ocnser.asp" -->
<%
id = request.form("id")
shuffle_order = request.form("shuffle_order")

if requestSafe(id) = 1 then
	id = 0
end if

Dim conn
call openConn()

SQL="SELECT parent_id FROM content WHERE id = " & id
set RecordSet = conn.Execute(SQL)
do until RecordSet.eof
	parent_id = RecordSet("parent_id")
	RecordSet.MoveNext
Loop
RecordSet.Close

submitStr = request.form("submit")

select case submitStr
	case "Re-order list"
		if requestSafe(shuffle_order) = 0 then
			shuf_s = -1
			SQL="SELECT id FROM content " &_
				"WHERE parent_id = " & parent_id & " " &_
				"AND id <> " & id & " " &_
				"AND shuffle_order >= " & shuffle_order & " " &_
				"ORDER BY shuffle_order ASC"
			Set RecordSet = conn.Execute(SQL)
			do while not RecordSet.EOF
				shuf_s = shuf_s + 1
				redim preserve shuffArr(shuf_s)
				shuffArr(shuf_s) = RecordSet("id")
				RecordSet.MoveNext
			loop
			RecordSet.Close
			shuffle_order = CInt(shuffle_order)
				for iShuf = 0 to shuf_s
				SQL="UPDATE content SET shuffle_order = " & shuffle_order + (iShuf + 1) & " " &_
				"WHERE id = " & shuffArr(iShuf)
				conn.Execute(SQL)
			next
			stkSQL = "UPDATE content SET shuffle_order = " & shuffle_order & " WHERE id = " & id
			conn.Execute(stkSQL)
		end if
end select

response.redirect "adm_content_list.asp?pid=" & parent_id
%>
