<% page_title = "Edit Photo Details" %>
<!-- #include file="adm_header.asp" -->
<%
file_id = request("img")
if file_id = "" then file_id = 0 end if

SQL="SELECT * FROM files WHERE file_id = " & file_id
set recordset = conn.execute(SQL)
do while not recordset.eof
	file_id = recordset("file_id")
	section_id = recordset("section_id")
	file_name = recordset("file_name")
	imgdesc = recordset("imgdesc")
	recordset.movenext
loop
recordset.close

gal_s = -1
SQL="SELECT * FROM section ORDER BY section_name"
set recordset = conn.execute(SQL)
do while not recordset.eof
	gal_s = gal_s + 1
	redim preserve gallArr(1,gal_s)
	gallArr(0,gal_s) = recordset("section_id")
	gallArr(1,gal_s) = recordset("section_name")
	recordset.movenext
loop
recordset.close

reDirStr = "doc_upload_individual.asp?id=1"
%>

<h4>File name: <%= file_name %></h4>
<table width="600">
<form name="form1" method="post" action="doc_upload_optionsCode.asp">
<input type="hidden" name="file_id" value="<%= file_id %>" />
<tr>
<td width="300" valign="top">
<h3>Uploaded Photo :</h3>
(Click to delete)<br />
<div style="padding: 3px;"><a href="#" onClick="return deleteUpload('<%= file_id %>&g=<%= section_id %>');">Delete File</a></div>
</td>
<td width="300" valign="top">

<div>
<%
for iGal = 0 to gal_s

	%><input type="hidden" value="<%= gallArr(0,iGal) %>" ><%
	response.write VbCrLf
next
%>
</div>
<h3>Carousel Image Title</h3>
<div>

<p><input name="title" size="60" value="<%= title %>" /></p>
<p><textarea name="imgdesc" cols="40" rows="10"><%= imgdesc %></textarea></p>
<p><input name="submit" type="submit" value="Update" /></p>
</div>
<div>&bull;&nbsp;&nbsp;<a href="<%= reDirStr %>">Return to Uploaded Photos</a></div>
</td>
</tr>
</form>
</table>

<!-- #include file="adm_footer.asp" -->