﻿<!-- #include file="adm_header.asp" -->
<%
iCon = -1
conSQL="SELECT section_id, section_name " &_
	   "FROM section " &_
	   "WHERE section_name <> '' " &_
	   "ORDER BY section_name, section_id"
set recordset = conn.execute(conSQL)
do while not recordset.eof
	iCon = iCon + 1
	redim preserve conArr(2,iCon)
	conArr(0,iCon) = recordset("section_id")
	conArr(1,iCon) = recordset("section_name")
	recordset.movenext
loop
recordset.close

page_title = "Document Sections"
%>
<h2 class="page-title">Vehicles</h2>
<p><a href="doc_upload_section_page.asp?id=new" class="button_text2">Add a New Vehicle</a></p>
<table>
<% for i = 0 to iCon %>
<tr>
<td><%= conArr(1,i) %></td>

<td><a href="doc_upload_section_page.asp?id=<%= conArr(0,i) %>">Edit Vehicle Details</a></td>
<td><a href="doc_upload_individual.asp?id=<%= conArr(0,i) %>" style="font-weight: normal;">Edit Vehicle Photos</a></td>
<td><a href="#" onClick="return deleteSection('<%= conArr(0,i) %>');" style="font-weight: normal;">Delete </a></td>
</tr>
<% next %>
</table>

<!-- #include file="adm_footer.asp" -->