function isBlank(lstrText)
{
	for (var i = 0; i < lstrText.length; i++)
	{
		var chr = lstrText.charAt(i);
		if (chr != ' ') return false;
	}
	return true;
}

function checkcontent( querystring )
{
	ErrorCheckStr = "";
	ErrorCheckVar = 0;


	if(isBlank(document.frmcontent.name.value))
	{
		ErrorCheckVar = 1;
	}
	else
	{
		ErrorCheckVar = ErrorCheckVar;
	}

	if(ErrorCheckVar > 0)
	{
		ErrorCheckStr = "You cannot leave the name field blank.";
		alert(ErrorCheckStr);
		return false;
	}
	else
	{
		return true;
	}
}

function deleteConfirm()
{
	confString = "You are about to permanently delete this content.\n\n";
	confString = confString + "Any sub-categories for this content will cease to appear anywhere on the site.\n \n";
	confString = confString + "Any products in this content or its sub-categories will cease to appear anywhere on the site.\n \n";
	confString = confString + "Are you sure you want to delete?";

	if(confirm(confString))
	{
		return true;
	}

	return false;
}