<%
response.buffer=true
Server.ScriptTimeout = 9999
Dim conn,rs,servicers, strConn
%>
<!-- #include file="include_authenticate.asp" -->
<!-- #include file="include_dbconnect.asp" -->
<!-- #include file="date_function.asp" -->
<%
call openConn()

function sqlSafe(passthru)
	if len(passthru)>0 then
		sqlSafe = replace(passthru,"'","&#146;")
	else
		sqlSafe = ""
	end if
end function

function requestSafe(passthru)
	stepthru = 0
	if IsEmpty(passthru) then
		stepthru = 1
	end if
	if IsNull(passthru) then
		stepthru = 1
	end if
	if passthru = "" then
		stepthru = 1
	end if
	if stepthru = 0 then
		if IsNumeric(CStr(passthru)) then
			stepthru = 0
		else
			stepthru = 1
		end if
	end if
	if len(passthru) > 8 then
		stepthru = 1
	end if
	requestSafe = stepthru
end function
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; ISO-8859-1" />
<title>Login  </title>
<link href="css/manager.css" rel="stylesheet" type="text/css" />
<link href="css/adminstyle.css" rel="stylesheet" type="text/css" />
<script language="JavaScript" type="text/JavaScript">
<!--
function deleteCWUser(querystring)
{
	input_box=confirm("Click OK or delete this Credit Works user permanently\nor Cancel to keep it.");
	if (input_box==true)
	{
		// Output when OK is clicked
		window.location = "sec_securityDel.asp?user_id="+querystring
	}
}

function deleteTestimonial(querystring)
{
	input_box=confirm("Click OK or delete this Testimonial permanently\nor Cancel to keep it.");
	if (input_box==true)
	{
		// Output when OK is clicked
		window.location = "testimonials_delete.asp?id="+querystring
	}
}
function deleteFAQ(querystring)
{
	input_box=confirm("Click OK or delete this FAQ permanently\nor Cancel to keep it.");
	if (input_box==true)
	{
		// Output when OK is clicked
		window.location = "faq_delete.asp?id="+querystring
	}
}
function deleteUpload(querystring)
{
	input_box=confirm("Click OK or delete this file permanently\nor Cancel to keep it.");
	if (input_box==true)
	{
		// Output when OK is clicked
		window.location = "doc_upload_delete.asp?img="+querystring
	}
}
function deleteImage(querystring)
{
	input_box=confirm("Click OK or delete this file permanently\nor Cancel to keep it.");
	if (input_box==true)
	{
		// Output when OK is clicked
		window.location = "adm_upload_delete.asp?img="+querystring
	}
}
function deleteNews(querystring)
{
	input_box=confirm("Click OK or delete this news article permanently\nor Cancel to keep it.");
	if (input_box==true)
	{
		// Output when OK is clicked
		window.location = "adm_news_delete.asp?id="+querystring
	}
}
function deleteSection(querystring)
{
	input_box=confirm("Click OK or delete this document section permanently\nor Cancel to keep it.");
	if (input_box==true)
	{
		// Output when OK is clicked
		window.location = "doc_upload_section_delete.asp?g="+querystring
	}
}
function deleteCategory(querystring)
{
	input_box=confirm("Click OK or delete this document section permanently\nor Cancel to keep it.");
	if (input_box==true)
	{
		// Output when OK is clicked
		window.location = "adm_section_delete.asp?g="+querystring
	}
}
function deleteSize(querystring)
{
	input_box=confirm("Click OK or delete this size permanently\nor Cancel to keep it.");
	if (input_box==true)
	{
		// Output when OK is clicked
		window.location = "adm_size_deletecode.asp?id="+querystring
	}
}
function deletePType(querystring)
{
	input_box=confirm("Click OK or delete this product type permanently\nor Cancel to keep it.");
	if (input_box==true)
	{
		// Output when OK is clicked
		window.location = "adm_ptype_deletecode.asp?id="+querystring
	}
}
function deleteGroup(querystring)
{
	input_box=confirm("Click OK or delete this group permanently\nor Cancel to keep it.");
	if (input_box==true)
	{
		// Output when OK is clicked
		window.location = "cms_group_deletecode.asp?id="+querystring
	}
}
function fireUpload()
{
	document.getElementById("upForm").style.display = "none";
	document.getElementById("upWarning").style.display = "block";
	return true;
}

function deleteUnit(querystring)
{
	input_box=confirm("Click OK or delete this unit permanently\nor Cancel to keep it.");
	if (input_box==true)
	{
		// Output when OK is clicked
		window.location = "course_del.asp?id="+querystring
	}
}
function deleteNewUnit(querystring)
{
	input_box=confirm("Click OK or delete this unit permanently\nor Cancel to keep it.");
	if (input_box==true)
	{
		// Output when OK is clicked
		window.location = "course_new_del.asp?id="+querystring
	}
}
function deleteQual(querystring)
{
	input_box=confirm("Click OK or delete this qualification permanently\nor Cancel to keep it.");
	if (input_box==true)
	{
		// Output when OK is clicked
		window.location = "qual_del.asp?id="+querystring
	}
}

-->
</script>

<!--! begin scripts for IE Rounded corners -->
<script src="js/DD_roundies_0.0.2a-min.js"> </script>

<script>


  /* IE only */
DD_roundies.addRule('.roundify', '20px 20px 20px 20px');
DD_roundies.addRule('.roundify2', '50px 50px 50px 50px');
DD_roundies.addRule('.roundify3', '9px 9px 0 0');
DD_roundies.addRule('.roundify4', '9px 9px 0 0');
  /* varying radii, IE only */
  DD_roundies.addRule('.something_else', '10px 4px');

  /* varying radii, "all" browsers */
  DD_roundies.addRule('.yet_another', '5px', true);
</script>



</head>

<body>
<!-- #include file="fckeditor.asp" -->

<div id="wrapper">


 
	<div id="header2">
    
    <img src="../../images/nh/logo.png" />
    <%
if InStr(request.servervariables("script_name"),"index.asp")=0 and InStr(request.servervariables("script_name"),"logout.asp")=0 then
	%>


<div id="top-nav">	<!--#include file="include_control.asp" --></div><%
end if
%>

</div>
<!-- end #header -->
<div id="content-surround-login" >
<% ' ************************************************************************************************************************ ' %>

<% ' ************************************************************************************************************************ ' %>
<div id="primary-content-login">
<h2 class="page-title"> CMS Login</h2>
<!-- start #mainContent -->