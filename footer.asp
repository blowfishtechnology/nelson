

		<div class="nh-containerfooter"><!-- Start of containerfooter -->
<div class="nh-footercenter"><!-- Start of footercenter -->
		
				<div class="nh-footerleft">
					<p>
						&copy; <% Response.Write Year(now) %>, Nelson Hydraulics Ltd. All rights reserved.
					</p>
				</div>
				
				<div class="nh-footerright">
					<p>
						<a href="http://www.websolutionsni.com" target="_blank">Designed &amp; Hosted by Web Solutions NI</a>
					</p>
				</div>
				
			</div><!-- End of footercenter -->
		
		</div><!-- End of containerfooter -->
		
	</div><!-- End of containercenter -->

</div><!-- End of container -->

</body>
</html>
